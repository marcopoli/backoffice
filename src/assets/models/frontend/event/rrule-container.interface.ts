import { RRule, RRuleSet } from 'rrule';

export interface RRuleContainer {
  rruleString: string;
  rruleObj: RRule;
  rruleSet: RRuleSet;
}
