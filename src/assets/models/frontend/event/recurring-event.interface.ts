import { RRuleContainer } from './rrule-container.interface';

/**
 * @interface RecurringEvent
 * @property {string} duration: Event duration, formatted as HH:mm
 */
export interface RecurringEvent {
  id: string;
  title: string;
  backgroundColor: string;
  duration: string;
  rrule: string;
  rruleContainer: RRuleContainer;
}
