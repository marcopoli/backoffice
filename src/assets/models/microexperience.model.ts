import { Availability } from './utility/availability.model';
import { BasicInfo } from './utility/basic-info.model';
import { Localization } from './utility/localization.model';

export interface Microexperience {
    _id?: string;
    basicInfo: BasicInfo;
    categories: {
        categoryId: string;
        weigth: number;
    }[];
    availability: Availability[];
    description: Localization[];
}
