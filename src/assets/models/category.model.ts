import { Localization } from './utility/localization.model';

export interface Category {
    _id: string;
    name: Localization[];
    imageSrc: string;
}
