export interface Localization {
    locale: string;
    name?: string;
    shortDesc: string;
    longDesc: string;
}
