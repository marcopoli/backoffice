export interface Availability {
  title: string;
  rrule: {
    frequency: number;
    start: string;
    end: string;
    whichDays: number[];
    excludedDays: string[];
  };
  recurrencies: string[];
}
