import { Coord } from './coord.model';

export interface BasicInfo {
  title: string;
  address: string;
  coords: Coord;
  angelId: string;
  qrCodeId: string;
  duration: string;
  image: string;
  gallery: string[];
}
