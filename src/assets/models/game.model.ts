import { Microexperience } from './microexperience.model';

export interface Game {
  _id?: string;
  itineraryId: string;
  goal: Goal[];
}

export interface Goal {
  type: 'experience';
  entity: Microexperience;
  score?: number;
  played: boolean;
}
