export interface Review {
    _id: string;
    createdAt: string;
    terminalId: string;
    guestId: string;
    microexperienceId: string;
    rating: number;
    comment: string;
}
