import { AuthData } from './utility/auth-data.model';

export interface Guest {
    _id: string;
    surname: string;
    name: string;
    birthDate: string;
    phoneNumber: string;
    imageUrl: string;
    angelId?: string;
    authData?: AuthData;
}
