import { Localization } from './utility/localization.model';
import { AuthData } from './utility/auth-data.model';

export interface Angel {
    _id: string;
    surname: string;
    name: string;
    birthDate: string;
    phoneNumber: string;
    imageUrl: string;
    description: Localization[];
    authData?: AuthData;
}
