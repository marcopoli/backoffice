export interface Itinerary {
  _id: string;
  createdAt: string;
  options: ItineraryOptions;
  creatorId: string;
  playerId: string;
  name?: string;
  saved: boolean;
  accepted: boolean;
}

export interface ItineraryOptions {
  categoryId: string;
  start: string;
  hoursAvailable: number;
}
