import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { AppComponent } from './app.component';
import { TokenManagerService } from './services/token-manager.service';
import { LocalStorageManager } from './services/local-storage-manager.service';
import { AppendAuthenticationInterceptor } from './services/interceptors/append-authentication.interceptor.service';
import { TokenInterceptor } from './services/interceptors/token.interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthModule,
    AppRoutingModule
  ],
  providers: [
    TokenManagerService,
    LocalStorageManager,
    {
      // Interceptor for adding the Authentication header
      provide: HTTP_INTERCEPTORS,
      useClass: AppendAuthenticationInterceptor,
      multi: true
    },
    {
      // Interceptor for retrieve token from responses
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
