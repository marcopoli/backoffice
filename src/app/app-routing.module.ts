import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './services/guards/auth.guard';
import { LoginBlockGuard } from './services/guards/login-block.guard';

const routes: Routes = [
    {
        path: 'auth',
        component: AuthComponent,
        canActivate: [LoginBlockGuard]
    },
    {
        path: '',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        canActivate: [AuthGuard]
    },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
