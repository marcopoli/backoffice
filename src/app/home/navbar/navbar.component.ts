import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SidebarService } from '../../services/utility/sidebar.service';
import { TokenManagerService } from 'src/app/services/token-manager.service';
import { LocalStorageManager } from 'src/app/services/local-storage-manager.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  username: string;
  isSidebarOpened: boolean;
  subscription: Subscription;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private tokenManager: TokenManagerService,
    private localStorageManager: LocalStorageManager
  ) { }

  ngOnInit() {
    this.username = this.localStorageManager.getUsername();
    this.isSidebarOpened = this.sidebarService.getIsSidebarOpened();
    this.subscription = this.sidebarService.isSidebarToogled.subscribe(() => this.isSidebarOpened = !this.isSidebarOpened);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onLogout() {
    this.tokenManager.destroyTokenData();
    this.localStorageManager.removeUsername();
    this.router.navigateByUrl('/auth');
  }

  onToogleSidebar() {
    this.sidebarService.setIsSidebarOpened();
  }

}
