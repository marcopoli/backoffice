import { Component, OnInit } from '@angular/core';
import { CONSTANTS } from 'src/assets/consts/app.const';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  partners = '';
  project: string;
  year: string;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.project = CONSTANTS.project;
    this.year = CONSTANTS.year;
    this.initializePartners();
  }

  initializePartners() {
    CONSTANTS.partners.forEach((partner, index) => {
      this.partners += partner;

      if (index < CONSTANTS.partners.length - 1) {
        this.partners += ' - ';
      }
    });
  }

}
