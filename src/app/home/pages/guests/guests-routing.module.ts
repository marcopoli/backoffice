import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestsComponent } from './guests.component';
import { GuestEditComponent } from './guest-edit/guest-edit.component';

const routes: Routes = [
    { path: '', component: GuestsComponent },
    { path: 'create', component: GuestEditComponent },
    { path: 'edit/:id', component: GuestEditComponent },
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GuestsRoutingModule { }
