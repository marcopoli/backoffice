import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Guest } from 'src/assets/models/guest.model';
import { GuestStorageService } from 'src/app/services/data-storage-services/guest-storage.service';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-guest-edit',
  templateUrl: './guest-edit.component.html',
  styleUrls: ['./guest-edit.component.css'],
})
export class GuestEditComponent implements OnInit, OnDestroy {
  /**
   * Guest element to be created/modified
   *
   * @type {Guest}
   * @memberof GuestEditComponent
   */
  guest: Guest;
  /**
   * Identifier of the guest to be modified
   *
   * @type {string}
   * @memberof GuestEditComponent
   */
  id: string;
  /**
   * Form with guest data
   *
   * @type {FormGroup}
   * @memberof GuestEditComponent
   */
  form: FormGroup;

  /**
   * Variable holding the name of image to be uploaded
   *
   * @type {File}
   * @memberof AngelEditComponent
   */
  imageFile: string;
  /**
   * Variable holding the images to be uploaded
   *
   * @type {FileList}
   * @memberof AngelEditComponent
   */
  uploadedFile: File;
  email: string;
  angelId: string;

  canSave: boolean;
  subscriptions: Subscription[];

  datePicker;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private guestStorageService: GuestStorageService,
    private imageManager: ImageManageService,
    private datePickerService: DatePickerService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.subscriptions = [];
      this.getGuest();
    });

    this.initDatePicker();
    this.buildForm();
    this.canSave = false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  goBack() {
    this.router.navigateByUrl('guests');
  }

  retrieveFile(event) {
    this.uploadedFile = event.target.files.item(0);
    this.imageFile = this.uploadedFile.name;
  }

  onRemoveImage() {
    this.uploadedFile = undefined;
    this.imageFile = '';
  }

  onSubmit() {
    const guest: Guest = this.form.value;

    // Check if it creation mode (with an uploaded file) or if profile image has been changed
    const guestImage = _.get(this.guest, 'imageUrl', '');

    // Upload profile image first
    const subscription = this.imageManager
      .manageImageFile(guestImage, this.uploadedFile, 'guests')
      .subscribe(
        (data: { message: string; imageSrc: string }) => {
          guest.imageUrl = data.imageSrc;
          if (data.message) {
            this.commonUtilityService.showMessage(data.message);
          }
        },
        (error) => {
          const message = 'Image upload: ';
          this.commonUtilityService.showMessage(message + error);
        }
      )
      .add(() => {
        this.updateGuest(guest);
      });

    this.subscriptions.push(subscription);
  }

  private getGuest() {
    const subscription = this.guestStorageService.readGuest(this.id).subscribe(
      (data: Guest) => {
        this.guest = data;
        this.email = _.get(this.guest, 'authData.email');
        this.angelId = this.guest.angelId;
        this.uploadedFile =
            this.guest.imageUrl === ''
              ? undefined
              : new File([], this.getFileName(this.guest.imageUrl));
        this.patchValues();
      },
      (httpError: HttpErrorResponse) => {
        this.goBack();
        this.commonUtilityService.showMessage(httpError.error.message);
      }
    );

    this.subscriptions.push(subscription);
  }

  private getFileName(imageSrc: string) {
    return _.last(imageSrc.split('/'));
  }

  private updateGuest(guest: Guest) {
    const subscription = this.guestStorageService
      .updateGuest(guest, this.id)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.goBack();
        },
        (httpError: HttpErrorResponse) =>
          this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  /**
   * Date picker initialization
   *
   * @private
   * @memberof GuestEditComponent
   */
  private initDatePicker() {
    this.datePicker = this.datePickerService.initializeDatePicker(
      '#date',
      new Date(1940, 0, 1),
      new Date()
    );
    const subscripton = this.datePickerService
      .registerForChanges(this.datePicker)
      .subscribe((date) => this.form.patchValue({ birthDate: date }));
    this.subscriptions.push(subscripton);
  }

  /**
   * Build form and initialize it
   *
   * @private
   * @memberof GuestEditComponent
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      birthDate: [''],
      phoneNumber: ['', Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
    });
  }

  /**
   * Form initialization if the page is in edit mode
   *
   * @private
   * @memberof GuestEditComponent
   */
  private patchValues() {
    this.form.patchValue(this.guest);
    this.imageFile =
      this.guest.imageUrl && this.guest.imageUrl !== ''
        ? this.imageManager.getFileName(this.guest.imageUrl)
        : '';
  }
}
