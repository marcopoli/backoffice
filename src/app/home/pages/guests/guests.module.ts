import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { GuestsRoutingModule } from './guests-routing.module';
import { DataStorageModule } from 'src/app/services/data-storage-services/data-storage.module';
import { GuestsComponent } from './guests.component';
import { GuestEditComponent } from './guest-edit/guest-edit.component';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';

@NgModule({
  declarations: [
    GuestsComponent,
    GuestEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    GuestsRoutingModule,
    DataStorageModule
  ],
  providers: [
    ImageManageService,
    DatePickerService
  ],
  bootstrap: []
})
export class GuestsModule { }
