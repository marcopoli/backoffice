import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Guest } from 'src/assets/models/guest.model';
import { GuestStorageService } from 'src/app/services/data-storage-services/guest-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';

@Component({
  selector: 'app-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.css'],
})
export class GuestsComponent implements OnInit, OnDestroy {
  @ViewChild('checkboxSelection', { static: true })
  checkboxSelection: ElementRef;

  guests: Guest[];
  subscriptions: Subscription[];
  allSelected: boolean;

  constructor(
    private router: Router,
    private guestStorageService: GuestStorageService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onDeleteAll() {
    const subscription = this.guestStorageService.deleteGuests().subscribe(
      (data: string) => {
        this.commonUtilityService.showMessage(data);
        this.init();
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  onDelete(index: number) {
    const id = this.guests[index]._id;
    const subscription = this.guestStorageService.deleteGuest(id).subscribe(
      (data: string) => {
        this.commonUtilityService.showMessage(data);
        this.init();
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  onModify(index: number) {
    const id = this.guests[index]._id;
    this.router.navigateByUrl('/guests/edit/' + id);
  }

  onUpdate() {
    this.init();
  }

  /**
   * Set all guest checkboxes 'checked' status to the same as the overall input checkbox
   *
   * @memberof GuestsComponent
   */
  onSelectAll() {
    this.allSelected = this.checkboxSelection.nativeElement.checked;
    this.guests.forEach((guest: Guest, index: number) => {
      const inputElement = document.getElementById(
        'checkbox' + index
      ) as HTMLInputElement;
      inputElement.checked = this.allSelected;
    });
  }

  private init() {
    this.guests = [];
    this.subscriptions = [];
    this.initCheckbox();
    this.getGuests();
  }

  private getGuests() {
    const subscription = this.guestStorageService.readGuests().subscribe(
      (data: Guest[]) => (this.guests = data),
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  private initCheckbox() {
    this.checkboxSelection.nativeElement.checked = false;
    this.allSelected = false;
  }
}
