import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Microexperience } from 'src/assets/models/microexperience.model';
import { MicroexperienceStorageService } from 'src/app/services/data-storage-services/microexperience-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';

@Component({
  selector: 'app-microexperiences',
  templateUrl: './microexperiences.component.html',
  styleUrls: ['./microexperiences.component.css']
})
export class MicroexperiencesComponent implements OnInit, OnDestroy {

  @ViewChild('checkboxSelection', { static: true }) checkboxSelection: ElementRef;

  microexperiences: Microexperience[];
  subscriptions: Subscription[];
  allSelected: boolean;

  constructor(
    private router: Router,
    private microexperienceStorageService: MicroexperienceStorageService,
    private commonUtilityService: CommonUtilityService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onDeleteAll() {
    const subscription = this.microexperienceStorageService.deleteMicroexperiences()
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.init();
        },
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  onDelete(index: number) {
    const id = this.microexperiences[index]._id;
    const subscription = this.microexperienceStorageService.deleteMicroexperience(id)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.init();
        },
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  onModify(index: number) {
    const id = this.microexperiences[index]._id;
    this.router.navigateByUrl('/microexperiences/edit/' + id);
  }

  onCreate() {
    this.router.navigateByUrl('/microexperiences/create');
  }

  onUpdate() {
    this.init();
  }

  /**
   * Set all microexperience checkboxes 'checked' status to the same as the overall input checkbox
   *
   * @memberof MicroexperiencesComponent
   */
  onSelectAll() {
    this.allSelected = this.checkboxSelection.nativeElement.checked;
    this.microexperiences.forEach((microexperience: Microexperience, index: number) => {
      const inputElement = document.getElementById('checkbox' + index) as HTMLInputElement;
      inputElement.checked = this.allSelected;
    });
  }

  private init() {
    this.microexperiences = [];
    this.subscriptions = [];
    this.initCheckbox();
    this.getMicroexperiences();
  }

  private initCheckbox() {
    this.checkboxSelection.nativeElement.checked = false;
    this.allSelected = false;
  }

  private getMicroexperiences() {
    const subscription = this.microexperienceStorageService.readMicroexperiences()
      .subscribe(
        (data: Microexperience[]) => this.microexperiences = data,
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

}
