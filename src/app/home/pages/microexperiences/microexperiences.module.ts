import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
import { ReactiveFormsModule } from '@angular/forms';
import { MicroexperiencesRoutingModule } from './microexperiences-routing.module';
import { DataStorageModule } from 'src/app/services/data-storage-services/data-storage.module';
import { MicroexperiencesComponent } from './microexperiences.component';
import { MicroexperienceEditComponent } from './microexperience-edit/microexperience-edit.component';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';
import { BasicInfoTabComponent } from './microexperience-edit/basic-info-tab/basic-info-tab.component';
import { CategoryTabComponent } from './microexperience-edit/category-tab/category-tab.component';
import { AvailabilityTabComponent } from './microexperience-edit/availability-tab/availability-tab.component';
import { MultilanguageTabComponent } from './microexperience-edit/multilanguage-tab/multilanguage-tab.component';
import { EventModalComponent } from './microexperience-edit/availability-tab/event-modal/event-modal.component';
import { AvailabilityHelperService } from './microexperience-edit/availability-tab/availability-helper.service';

@NgModule({
  declarations: [
    MicroexperiencesComponent,
    MicroexperienceEditComponent,
    BasicInfoTabComponent,
    CategoryTabComponent,
    AvailabilityTabComponent,
    MultilanguageTabComponent,
    EventModalComponent,
  ],
  imports: [
    CommonModule,
    NgbModalModule,
    ReactiveFormsModule,
    IonRangeSliderModule,
    MicroexperiencesRoutingModule,
    DataStorageModule,
  ],
  providers: [ImageManageService, DatePickerService, AvailabilityHelperService],
  entryComponents: [EventModalComponent],
  bootstrap: [],
})
export class MicroexperiencesModule {}
