import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MicroexperiencesComponent } from './microexperiences.component';
import { MicroexperienceEditComponent } from './microexperience-edit/microexperience-edit.component';

const routes: Routes = [
    { path: '', component: MicroexperiencesComponent },
    { path: 'create', component: MicroexperienceEditComponent },
    { path: 'edit/:id', component: MicroexperienceEditComponent },
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MicroexperiencesRoutingModule { }
