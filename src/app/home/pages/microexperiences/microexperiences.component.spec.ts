import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroexperiencesComponent } from './microexperiences.component';

describe('MicroexperiencesComponent', () => {
  let component: MicroexperiencesComponent;
  let fixture: ComponentFixture<MicroexperiencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicroexperiencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroexperiencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
