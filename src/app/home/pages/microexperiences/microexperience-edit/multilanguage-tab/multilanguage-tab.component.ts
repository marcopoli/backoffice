import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Localization } from 'src/assets/models/utility/localization.model';

@Component({
  selector: 'app-multilanguage-tab',
  templateUrl: './multilanguage-tab.component.html',
  styleUrls: ['./multilanguage-tab.component.css']
})
export class MultilanguageTabComponent implements OnInit, OnChanges {

  @Input() isEditing: boolean;
  @Input() multilanguage: Localization[];
  @Output() multilanguageChange = new EventEmitter<Localization[]>();
  @Output() multilanguageValid = new EventEmitter<boolean>();

  canSave: boolean;

  constructor() { }

  ngOnInit() {
    this.emitChanges();
  }

  ngOnChanges() {
    this.emitChanges();
  }

  /**
   * Update name array variable with ones typed by user
   *
   * @param {*} event
   * @param {number} index
   * @memberof MultilanguageTabComponent
   */
  updateName(event, index: number) {
    this.multilanguage[index].name = event.target.value;
    this.emitChanges();
  }

  /**
   * Update short description array variable with ones typed by user
   *
   * @param {*} event
   * @param {number} index
   * @memberof MultilanguageTabComponent
   */
  updateShortDescription(event, index: number) {
    this.multilanguage[index].shortDesc = event.target.value;
    this.emitChanges();
  }

  /**
   * Update long description array variable with ones typed by user
   *
   * @param {*} event
   * @param {number} index
   * @memberof MultilanguageTabComponent
   */
  updateLongDescription(event, index: number) {
    this.multilanguage[index].longDesc = event.target.value;
    this.emitChanges();
  }

  private emitChanges() {
    this.multilanguageChange.emit(this.multilanguage);
    this.multilanguageValid.emit(this.verifyCanSave());
  }

  private verifyCanSave() {
    if (this.multilanguage) {
      const uncompleted = this.multilanguage.filter(localized => {
        const nameInserted = localized.name !== '';
        const shortDescInserted = localized.shortDesc !== '';

        return (nameInserted && !shortDescInserted) || (!nameInserted && shortDescInserted);
      });

      this.canSave = uncompleted.length === 0;
    }

    return this.canSave;
  }

}
