import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultilanguageTabComponent } from './multilanguage-tab.component';

describe('MultilanguageTabComponent', () => {
  let component: MultilanguageTabComponent;
  let fixture: ComponentFixture<MultilanguageTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultilanguageTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultilanguageTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
