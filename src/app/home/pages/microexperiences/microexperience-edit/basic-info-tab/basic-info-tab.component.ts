import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnDestroy,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { AngelStorageService } from 'src/app/services/data-storage-services/angel-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';
import { DateService } from 'src/app/services/utility/date.service';
import { BasicInfo } from 'src/assets/models/utility/basic-info.model';
import { Angel } from 'src/assets/models/angel.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-basic-info-tab',
  templateUrl: './basic-info-tab.component.html',
  styleUrls: ['./basic-info-tab.component.css'],
})
export class BasicInfoTabComponent implements OnInit, OnChanges, OnDestroy {
  @Input() isEditing: boolean;
  @Input() data: BasicInfo;
  @Input() image: File;
  @Input() gallery: File[];
  @Output() dataChange = new EventEmitter<BasicInfo>();
  @Output() imageChange = new EventEmitter<File>();
  @Output() galleryChange = new EventEmitter<File[]>();
  @Output() basicInfoValid = new EventEmitter<boolean>();

  form: FormGroup;
  allAngels: Angel[];

  subscriptions: Subscription[];

  constructor(
    private formBuilder: FormBuilder,
    private angelStorageService: AngelStorageService,
    private dateService: DateService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.subscriptions = [];
    this.buildForm();
    this.retrieveAngels();
  }

  ngOnChanges(changes: SimpleChanges) {
    const dataChanged = changes.data;

    if (
      dataChanged?.previousValue !== undefined &&
      !_.isEqual(dataChanged?.previousValue, dataChanged?.currentValue)
    ) {
      this.initData();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subcription) => subcription.unsubscribe());
  }

  retrieveFile(event) {
    const files = event.target.files;
    this.image = files.item(0) === null ? undefined : files.item(0);
    this.emitChanges();
  }

  onRemoveImage() {
    this.image = undefined;
    this.emitChanges();
  }

  updateGallery(event, index: number) {
    const files = event.target.files;
    files.item(0) === null
      ? this.gallery.splice(index, 1)
      : this.gallery.splice(index, 1, files.item(0));
    this.emitChanges();
  }

  onRemoveGalleryImage(index: number) {
    this.gallery.splice(index, 1);
    this.emitChanges();
  }

  onAddGalleryimage() {
    this.gallery.push(undefined);
    this.emitChanges();
  }

  private retrieveAngels() {
    const subscription = this.angelStorageService.readAngels().subscribe(
      (data: Angel[]) => {
        this.allAngels = data;
        if (this.data !== undefined) {
          this.initData();
        }
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  /**
   * Build form and initialize it
   *
   * @private
   * @memberof BasicInfoTabComponent
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      address: ['', Validators.required],
      coords: this.formBuilder.group({
        latitude: [
          '',
          Validators.compose([
            Validators.min(-90),
            Validators.max(90),
            Validators.required,
          ]),
        ],
        longitude: [
          '',
          Validators.compose([
            Validators.min(-180),
            Validators.max(180),
            Validators.required,
          ]),
        ],
      }),
      angelId: ['', Validators.required],
      qrCodeId: [
        '',
        Validators.compose([
          Validators.minLength(10),
          Validators.maxLength(10),
        ]),
      ],
      duration: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^(2[0-3]|[01][0-9]):([0-5][0-9])$'),
        ]),
      ],
    });

    this.image = undefined;
    this.gallery = [];
  }

  private initData() {
    if (this.isEditing) {
      this.patchValues();
    }

    this.emitChanges();
    this.form.valueChanges.subscribe(() => {
      this.emitChanges();
    });
  }

  /**
   * Form initialization if the page is in edit mode
   *
   * @private
   * @memberof BasicInfoTabComponent
   */
  private patchValues() {
    this.form.patchValue(this.data);

    if (this.data.duration) {
      const validatedHour = this.dateService.parseHourWithValidation(
        this.data.duration
      );
      const duration = this.dateService.formatHour(validatedHour);
      this.form.patchValue({
        duration: duration,
      });
    }
  }

  /**
   * Build the basic info object and emit the updated object to upper component
   *
   * @private
   * @memberof BasicInfoTabComponent
   */
  private emitChanges() {
    this.dataChange.emit(this.buildInfo());
    this.imageChange.emit(this.image);
    this.galleryChange.emit(this.gallery);
    this.basicInfoValid.emit(this.form.valid);
  }

  /**
   * Build the basic info of the experience from data collected
   *
   * @private
   * @returns {BasicInfo}
   * @memberof BasicInfoTabComponent
   */
  private buildInfo(): BasicInfo {
    const formValue = this.form.value;

    this.data = {
      title: formValue.title,
      address: formValue.address,
      coords: formValue.coords,
      angelId: formValue.angelId,
      qrCodeId: formValue.qrCodeId,
      duration: formValue.duration,
      image: this.data.image,
      gallery: this.data.gallery,
    };

    return this.data;
  }
}
