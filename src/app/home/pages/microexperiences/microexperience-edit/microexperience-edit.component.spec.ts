import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroexperienceEditComponent } from './microexperience-edit.component';

describe('MicroexperienceEditComponent', () => {
  let component: MicroexperienceEditComponent;
  let fixture: ComponentFixture<MicroexperienceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicroexperienceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroexperienceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
