import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';

export const UntilDateValidator: ValidatorFn = (
  formGroup: FormGroup
): ValidationErrors | null => {
  const date = formGroup.get('date');
  const until = formGroup.get('until');

  const dateMoment = moment(date.value, 'DD/MM/YYYY');
  const untilMoment = moment(until.value, 'DD/MM/YYYY');

  return dateMoment.isSameOrBefore(untilMoment)
    ? null
    : { untilNotValid: true };
};
