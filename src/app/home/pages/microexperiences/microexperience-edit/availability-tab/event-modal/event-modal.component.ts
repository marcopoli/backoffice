import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';
import { DateService } from 'src/app/services/utility/date.service';
import { RecurringEvent } from 'src/assets/models/frontend/event/recurring-event.interface';
import { UntilDateValidator } from './until-date-validator.directive';
import { AvailabilityHelperService } from '../availability-helper.service';

@Component({
  selector: 'app-event-modal',
  templateUrl: './event-modal.component.html',
  styleUrls: ['./event-modal.component.css'],
})
export class EventModalComponent implements OnInit, OnDestroy {
  @Input() event: RecurringEvent;
  @Input() dayClicked: Date;

  form: FormGroup;
  subscriptions: Subscription[];

  startDatePicker;
  untilDatePicker;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private datePickerService: DatePickerService,
    private dateService: DateService,
    private helperService: AvailabilityHelperService
  ) {}

  ngOnInit() {
    this.subscriptions = [];
    this.initDatePicker();
    this.initUntilDatePicker();
    this.buildForm();
    this.patchValue();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onClose() {
    this.activeModal.close();
  }

  onCancelOne() {
    const rruleSet = this.helperService.excludeDate(
      this.event.rruleContainer.rruleSet,
      this.dayClicked
    );
    this.event.rruleContainer = {
      rruleSet: rruleSet,
      rruleObj: rruleSet._rrule[0],
      rruleString: rruleSet.toString(),
    };
    this.event.rrule = rruleSet.toString();
    this.activeModal.close(this.event);
  }

  onCancelAll() {
    this.activeModal.dismiss();
  }

  onSubmit() {
    const value = this.form.value;
    const days =
      value.whichDays === '' ? [] : value.whichDays.map((day) => +day);
    const rrule = this.helperService.createRRule(
      this.dateService.parseDateHourWithValidationUTC(
        value.date + ' - ' + value.hour
      ),
      this.dateService.parseDateHourWithValidationUTC(value.until + ' - 23:59'),
      days
    );

    this.event.title = value.title;
    this.event.rrule = rrule.rruleString;
    this.event.rruleContainer = rrule;

    this.activeModal.close(this.event);
  }

  private initDatePicker() {
    this.startDatePicker = this.datePickerService.initializeDatePicker(
      '#date',
      new Date()
    );
    const subscripton = this.datePickerService
      .registerForChanges(this.startDatePicker)
      .subscribe((date) => this.form.patchValue({ date: date }));

    this.subscriptions.push(subscripton);
  }

  private initUntilDatePicker() {
    this.untilDatePicker = this.datePickerService.initializeDatePicker(
      '#until',
      new Date()
    );
    const subscripton = this.datePickerService
      .registerForChanges(this.untilDatePicker)
      .subscribe((date) => this.form.patchValue({ until: date }));

    this.subscriptions.push(subscripton);
  }

  private buildForm() {
    this.form = this.formBuilder.group(
      {
        title: ['', Validators.required],
        date: ['', Validators.required],
        hour: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^(2[0-3]|[01][0-9]):([0-5][0-9])$'),
          ]),
        ],
        until: ['', Validators.required],
        whichDays: [''],
      },
      { validators: UntilDateValidator }
    );
  }

  private patchValue() {
    const options = this.event.rruleContainer.rruleObj.options;
    const days = options.byweekday
      ? options.byweekday.map((day) => day.toString())
      : '';

    this.form.patchValue({
      title: this.event.title,
      date: this.dateService.formatDateUTC(options.dtstart),
      hour: this.dateService.formatHourUTC(options.dtstart),
      until: this.dateService.formatDateUTC(options.until),
      whichDays: days,
    });
  }
}
