import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
  OnChanges,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import { Calendar, OptionsInput, EventApi } from '@fullcalendar/core';
import itLocale from '@fullcalendar/core/locales/it';
import { ToolbarInput } from '@fullcalendar/core/types/input-types';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import rrulePlugin from '@fullcalendar/rrule';
import timeGridPlugin from '@fullcalendar/timegrid';
import { EventModalComponent } from './event-modal/event-modal.component';
import { AvailabilityHelperService } from './availability-helper.service';
import { DateService } from 'src/app/services/utility/date.service';
import { Availability } from 'src/assets/models/utility/availability.model';
import { RecurringEvent } from 'src/assets/models/frontend/event/recurring-event.interface';
import * as _ from 'lodash';

@Component({
  selector: 'app-availability-tab',
  templateUrl: './availability-tab.component.html',
  styleUrls: ['./availability-tab.component.css'],
})
export class AvailabilityTabComponent
  implements OnInit, OnChanges, AfterViewInit {
  @ViewChild('calendarElement') calendarElement: ElementRef;
  @ViewChild('eventModal') eventModal: NgbModal;

  @Input() experienceTitle: string;
  @Input() duration: string;
  @Input() slots: Availability[];
  @Output() slotsChange = new EventEmitter<Availability[]>();

  counterId: number;

  calendar: Calendar;
  calendarPlugins = [
    dayGridPlugin,
    timeGridPlugin,
    bootstrapPlugin,
    interactionPlugin,
    rrulePlugin,
  ];
  header: ToolbarInput = {
    left: 'prevYear,prev,today,next,nextYear',
    center: 'title',
    right: 'dayGridMonth,timeGridWeek',
  };
  calendarOptions: OptionsInput = {
    themeSystem: 'bootstrap',
    locale: itLocale,
    plugins: this.calendarPlugins,
    header: this.header,
    eventTimeFormat: (obj) => this.dateService.formatHourUTC(obj.start.marker),
    // automatically define calendar height
    height: 'auto',
    // hide the 'all-day' slot from week view (timegrid view)
    allDaySlot: false,
    // frequency for displaying time slots
    slotDuration: '00:30:00',
    // allow to drag events on calendar
    editable: false,
    // allow to highlight multiple days
    selectable: false,
    // event fired when clicked on a date
    dateClick: (info) => this.dayClick(info.date),
    // event fired when clicked on an event
    eventClick: (info) => this.eventClick(info.event),
  };

  constructor(
    private modalService: NgbModal,
    private availabilityHelper: AvailabilityHelperService,
    private dateService: DateService
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnChanges() {
    this.init();
  }

  ngAfterViewInit() {
    this.calendar = new Calendar(
      this.calendarElement.nativeElement,
      this.calendarOptions
    );
    this.calendar.render();
  }

  private init() {
    if (this.slots !== undefined && this.calendar) {
      this.calendar.removeAllEventSources();
      this.counterId = 0;
      this.createEvents();
    }
  }

  /**
   * Convert the avalilability slots into event objects for the calendar
   *
   * @private
   * @memberof AvailabilityTabComponent
   */
  private createEvents() {
    // add events to calendar
    this.slots.forEach((slot) => {
      this.counterId++;
      const event = this.availabilityHelper.convertAvailabilityToRecurringEvent(
        slot,
        this.duration,
        this.counterId.toString()
      );
      this.calendar.addEventSource({
        id: this.counterId.toString(),
        events: [event],
      });
    });
  }

  private dayClick(date: Date) {
    const dayClicked = this.dateService.convertDateToUTC(date);
    // create recurrency options
    const rrule = this.availabilityHelper.createRRule(
      dayClicked,
      dayClicked,
      []
    );
    // create a single event on calendar
    const recurringEvent = this.availabilityHelper.createRecurringEvent(
      (this.counterId++).toString(),
      this.experienceTitle,
      this.duration,
      rrule
    );
    // create an event object for Availability
    this.slots.push(
      this.availabilityHelper.convertRecurringEventToAvailability(
        recurringEvent
      )
    );
    this.openModal(recurringEvent, this.slots.length - 1, dayClicked, 'new');
  }

  private eventClick(eventFromCalendar: EventApi) {
    const dayClicked = this.dateService.convertDateToUTC(
      eventFromCalendar.start
    );
    const recurringEvent = this.availabilityHelper.getRecurringEventFromEventSourceData(
      eventFromCalendar.source
    );
    const slotIndex = this.availabilityHelper.findAvailabilityIndexFromRecurringEvent(
      this.slots,
      recurringEvent
    );
    this.openModal(recurringEvent, slotIndex, dayClicked, 'edit');
  }

  private openModal(
    event: RecurringEvent,
    slotIndex: number,
    dayClicked: Date,
    mode: 'new' | 'edit'
  ) {
    const modalRef = this.modalService.open(EventModalComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true,
    });
    modalRef.componentInstance.event = _.clone(event);
    modalRef.componentInstance.dayClicked =
      mode === 'edit' ? dayClicked : undefined;

    modalRef.result.then(
      // click on save, delete single date or close button
      (result: RecurringEvent) => {
        // save or delete single date button clicked
        if (result) {
          if (mode === 'edit') {
            this.calendar.getEventSourceById(result.id).remove();
          }

          this.calendar.addEventSource({
            id: result.id,
            events: [result],
          });

          this.slots[
            slotIndex
          ] = this.availabilityHelper.convertRecurringEventToAvailability(
            result
          );
          this.emitChanges();
        }
      },
      // click on delete all button
      () => {
        this.calendar.getEventSourceById(event.id).remove();
        this.slots.splice(slotIndex, 1);
      }
    );
  }

  private emitChanges() {
    this.slotsChange.emit(this.slots);
  }
}
