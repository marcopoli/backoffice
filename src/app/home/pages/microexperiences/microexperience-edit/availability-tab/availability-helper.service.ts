import { Injectable } from '@angular/core';
import EventSourceApi from '@fullcalendar/core/api/EventSourceApi';
import { RRule, RRuleSet, Frequency } from 'rrule';
import { DateService } from 'src/app/services/utility/date.service';
import { Availability } from 'src/assets/models/utility/availability.model';
import { RecurringEvent } from 'src/assets/models/frontend/event/recurring-event.interface';
import { RRuleContainer } from 'src/assets/models/frontend/event/rrule-container.interface';
import * as _ from 'lodash';

@Injectable()
export class AvailabilityHelperService {
  constructor(private dateService: DateService) {}

  convertAvailabilityToRecurringEvent(
    availability: Availability,
    duration: string,
    id: string
  ): RecurringEvent {
    const rruleContainer = this.createRRule(
      this.dateService.convertStringToUTC(availability.rrule.start),
      this.dateService.convertStringToUTC(availability.rrule.end),
      availability.rrule.whichDays,
      availability.rrule.excludedDays.map((date) =>
        this.dateService.convertStringToUTC(date)
      )
    );
    return this.createRecurringEvent(
      id,
      availability.title,
      duration,
      rruleContainer
    );
  }

  convertRecurringEventToAvailability(
    recurringEvent: RecurringEvent
  ): Availability {
    const rruleOptions = recurringEvent.rruleContainer.rruleObj.options;
    const rruleSet = recurringEvent.rruleContainer.rruleSet;

    return {
      title: recurringEvent.title,
      rrule: {
        frequency: rruleOptions.freq,
        start: rruleOptions.dtstart.toISOString(),
        end: rruleOptions.until.toISOString(),
        whichDays: rruleOptions.byweekday,
        excludedDays: rruleSet._exdate.map((date) => date.toISOString()),
      },
      recurrencies: this.getRecurrencies(rruleSet),
    };
  }

  createRecurringEvent(
    id: string,
    title: string,
    duration: string,
    rrule: RRuleContainer
  ): RecurringEvent {
    return {
      id: id,
      title: title,
      backgroundColor: '#73bd73',
      duration: duration,
      rrule: rrule.rruleString,
      rruleContainer: rrule,
    };
  }

  createRRule(
    startDate: Date,
    endDate: Date,
    whichDays: number[],
    excludedDates?: Date[]
  ): RRuleContainer {
    const rrule = this.createRRuleObj(startDate, endDate, whichDays);

    let rruleSet = new RRuleSet();
    rruleSet.rrule(rrule);

    if (excludedDates) {
      excludedDates.forEach((date) => {
        rruleSet = this.excludeDate(rruleSet, date);
      });
    }

    return {
      rruleSet: rruleSet,
      rruleObj: rrule,
      rruleString: rruleSet.toString(),
    };
  }

  getRecurringEventFromEventSourceData(source: EventSourceApi): RecurringEvent {
    return source.internalEventSource.extendedProps.events[0];
  }

  excludeDate(rruleSet: RRuleSet, date: Date) {
    rruleSet.exdate(date);
    return rruleSet;
  }

  findAvailabilityIndexFromRecurringEvent(
    slots: Availability[],
    event: RecurringEvent
  ) {
    const toFind = this.convertRecurringEventToAvailability(event);
    return slots.findIndex((slot) => {
      const titleMatch = slot.title === toFind.title;
      const recMatch = _.isEqual(slot.recurrencies, toFind.recurrencies);
      return titleMatch && recMatch;
    });
  }

  private getRecurrencies(rruleSet: RRuleSet) {
    const allDates = rruleSet.all();
    const excludedDates = rruleSet._exdate;
    const recurrencies = _.differenceWith(allDates, excludedDates, _.isEqual);
    return recurrencies.map((date) => this.dateService.formatDateHourUTC(date));
  }

  private createRRuleObj(startDate: Date, endDate: Date, whichDays: number[]) {
    return new RRule({
      freq: Frequency.DAILY,
      dtstart: startDate,
      until: endDate,
      byweekday: whichDays,
    });
  }
}
