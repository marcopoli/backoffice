import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Category } from 'src/assets/models/category.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-category-tab',
  templateUrl: './category-tab.component.html',
  styleUrls: ['./category-tab.component.css']
})
export class CategoryTabComponent implements OnInit, OnChanges {

  @Input() isEditing: boolean;
  @Input() categories: { categoryId: string; weigth: number }[];
  @Input() allCategories: Category[];
  @Output() categoriesChange = new EventEmitter<{ categoryId: string; weigth: number }[]>();
  @Output() categoriesValid = new EventEmitter<boolean>();

  categorySelection: CategorySelectionVO[];

  min = 0;
  max = 100;
  value = 50;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes: SimpleChanges) {
    const categoriesChanged = changes.categories;

    if (categoriesChanged) {
      const previous = categoriesChanged.previousValue;
      const current = categoriesChanged.currentValue;

      if (!(_.isEqual(previous, current))) {
        this.init();
      }
    }
  }

  updateCategory(event, index: number) {
    this.categories[index].categoryId = event.target.value;
    this.emitChanges();
  }

  updateWeigth(event, index: number) {
    this.categories[index].weigth = event.from;
    this.emitChanges();
  }

  onAddCategory() {
    this.categories.push({
      categoryId: this.allCategories[0]._id,
      weigth: 0
    });
  }

  private init() {
    this.initData();
    this.createCategorySelectionVo();
    this.emitChanges();
  }

  private initData() {
    if ((this.categories === undefined || this.categories.length === 0) && this.allCategories.length > 0) {
      this.categories = [{
        categoryId: this.allCategories[0]._id,
        weigth: 0
      }];
    }
  }

  private createCategorySelectionVo() {
    this.categorySelection = [];
    this.allCategories.forEach(category => {
      let categoryName = '';
      const itNames = category.name.filter(name => name.locale === 'it');
      categoryName = itNames.length > 0 ? itNames[0].shortDesc : category.name[0].shortDesc;

      this.categorySelection.push({
        _id: category._id,
        name: categoryName
      });
    });
  }

  private emitChanges() {
    let validCategories = [];
    if (this.categories) {
      validCategories = this.categories.filter(category => {
        return category.weigth > 0;
      });
    }

    this.categoriesChange.emit(this.categories);
    this.categoriesValid.emit(validCategories.length > 0);
  }

}

interface CategorySelectionVO {
  _id: string;
  name: string;
}
