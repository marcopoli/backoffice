import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MicroexperienceStorageService } from 'src/app/services/data-storage-services/microexperience-storage.service';
import { CategoryStorageService } from 'src/app/services/data-storage-services/category-storage.service';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';
import { Microexperience } from 'src/assets/models/microexperience.model';
import { BasicInfo } from 'src/assets/models/utility/basic-info.model';
import { Availability } from 'src/assets/models/utility/availability.model';
import { Localization } from 'src/assets/models/utility/localization.model';
import { Category } from 'src/assets/models/category.model';
import { Coord } from 'src/assets/models/utility/coord.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-microexperience-edit',
  templateUrl: './microexperience-edit.component.html',
  styleUrls: ['./microexperience-edit.component.css'],
})
export class MicroexperienceEditComponent implements OnInit, OnDestroy {
  /**
   * Microexperience to be created/modified
   *
   * @type {Microexperience}
   * @memberof MicroexperienceEditComponent
   */
  experience: Microexperience;
  /**
   * Identifier of the angel to be modified
   *
   * @type {string}
   * @memberof MicroexperienceEditComponent
   */
  id: string;
  /**
   * Define if it is a creation or an editing
   *
   * @type {boolean}
   * @memberof MicroexperienceEditComponent
   */
  isEditing: boolean;

  allCategories: Category[];

  basicInfo: BasicInfo;
  categories: { categoryId: string; weigth: number }[];
  availability: Availability[];
  description: Localization[];

  basicInfoValid: boolean;
  categoriesValid: boolean;
  multilanguageValid: boolean;
  overallValid: boolean;

  image: File;
  gallery: File[];

  subcriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private microexperienceStorageService: MicroexperienceStorageService,
    private categoryStorageService: CategoryStorageService,
    private imageManager: ImageManageService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.allCategories = [];
      const subscription = this.categoryStorageService
        .readCategories()
        .subscribe(
          (data: Category[]) => (this.allCategories = data),
          (httpError: HttpErrorResponse) =>
            this.commonUtilityService.showMessage(httpError.error.message)
        )
        .add(() => {
          this.id = params['id'];
          this.isEditing = !!this.id;
          this.initValidation();
          this.isEditing ? this.getMicroexperience() : this.initExperience();
        });

      this.subcriptions.push(subscription);
    });
  }

  ngOnDestroy() {
    this.subcriptions.forEach((subscription) => subscription.unsubscribe());
  }

  goBack() {
    this.router.navigateByUrl('microexperiences');
  }

  onInfoValidation(valid: boolean) {
    this.basicInfoValid = valid;
    this.checkValidation();
  }

  onCategoriesValidation(valid: boolean) {
    this.categoriesValid = valid;
    this.checkValidation();
  }

  onMultilanguageValidation(valid: boolean) {
    this.multilanguageValid = valid;
    this.checkValidation();
  }

  onSave() {
    const categories = this.categories || [];
    this.experience = {
      basicInfo: this.basicInfo,
      availability: this.availability,
      categories: this.cleanCategories(categories),
      description: this.description,
    };

    // Handle image file
    const imageSubscription = this.imageManager
      .manageImageFile(
        this.experience.basicInfo.image,
        this.image,
        'microexperiences'
      )
      .subscribe(
        (data: { message: string; imageSrc: string }) => {
          this.experience.basicInfo.image = data.imageSrc;
          if (data.message) {
            this.commonUtilityService.showMessage(data.message);
          }
        },
        (error) => {
          const message = 'Image upload: ';
          this.commonUtilityService.showMessage(message + error);
        }
      )
      .add(() => {
        // Handle gallery images
        const gallerySubscription = this.imageManager
          .manageGalleryFiles(this.experience.basicInfo.gallery, this.gallery)
          .subscribe(
            (data: { message: string; uploadedGallery: string[] }) => {
              this.experience.basicInfo.gallery = data.uploadedGallery;
              if (data.message) {
                this.commonUtilityService.showMessage(data.message);
              }
            },
            (errorMessage: string) =>
              this.commonUtilityService.showMessage(errorMessage)
          )
          .add(() => {
            console.log(this.experience);
            this.completeUpload(this.experience);
          });

        this.subcriptions.push(gallerySubscription);
      });

    this.subcriptions.push(imageSubscription);
  }

  private completeUpload(experience: Microexperience) {
    this.isEditing
      ? this.updateMicroexperience(experience)
      : this.createMicroexperience(experience);
  }

  private getMicroexperience() {
    this.basicInfo = this.initBasicInfo();
    this.categories = this.initCategories();

    const subscription = this.microexperienceStorageService
      .readMicroexperience(this.id)
      .subscribe(
        (data: Microexperience) => {
          this.experience = data;
          this.basicInfo = this.experience.basicInfo;
          this.image =
            this.basicInfo.image === ''
              ? undefined
              : new File([], this.getFileName(this.basicInfo.image));
          this.gallery = [];
          this.basicInfo.gallery.forEach((galleryImage) => {
            this.gallery.push(new File([], this.getFileName(galleryImage)));
          });
          this.categories = this.experience.categories;
          this.availability = this.experience.availability;
          this.description = this.experience.description;
        },
        (httpError: HttpErrorResponse) => {
          this.goBack();
          this.commonUtilityService.showMessage(httpError.error.message);
        }
      );

    this.subcriptions.push(subscription);
  }

  private updateMicroexperience(experience: Microexperience) {
    const subscription = this.microexperienceStorageService
      .updateMicroexperience(experience, this.id)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.goBack();
        },
        (httpError: HttpErrorResponse) =>
          this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subcriptions.push(subscription);
  }

  private createMicroexperience(experience: Microexperience) {
    const subscription = this.microexperienceStorageService
      .createMicroexperience(experience)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.goBack();
        },
        (httpError: HttpErrorResponse) =>
          this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subcriptions.push(subscription);
  }

  private initExperience() {
    this.image = undefined;
    this.gallery = [];
    this.basicInfo = this.initBasicInfo();
    this.categories = this.initCategories();
    this.availability = [];
    this.description = this.initMultilanguage();

    this.experience = {
      basicInfo: this.basicInfo,
      categories: this.categories,
      availability: this.availability,
      description: this.description,
    };
  }

  private initBasicInfo(): BasicInfo {
    const coords: Coord = {
      latitude: undefined,
      longitude: undefined,
    };

    return {
      title: '',
      address: '',
      coords: coords,
      angelId: '',
      qrCodeId: '',
      duration: '00:05',
      image: '',
      gallery: [],
    };
  }

  private initCategories() {
    return [
      {
        categoryId: _.get(this.allCategories, '[0]._id', ''),
        weigth: 0,
      },
    ];
  }

  private initMultilanguage() {
    return [
      {
        locale: 'it',
        name: '',
        shortDesc: '',
        longDesc: '',
      },
      {
        locale: 'en',
        name: '',
        shortDesc: '',
        longDesc: '',
      },
    ];
  }

  private initValidation() {
    this.basicInfoValid = false;
    this.categoriesValid = false;
    this.overallValid = false;
  }

  private checkValidation() {
    this.overallValid =
      this.basicInfoValid && this.categoriesValid && this.multilanguageValid;
  }

  private cleanCategories(
    categories: { categoryId: string; weigth: number }[]
  ) {
    return categories.filter(
      (category: { categoryId: string; weigth: number }) => {
        return category.weigth !== 0;
      }
    );
  }

  private getFileName(imageSrc: string) {
    return _.last(imageSrc.split('/'));
  }
}
