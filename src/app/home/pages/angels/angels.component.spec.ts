import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngelsComponent } from './angels.component';

describe('AngelsComponent', () => {
  let component: AngelsComponent;
  let fixture: ComponentFixture<AngelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
