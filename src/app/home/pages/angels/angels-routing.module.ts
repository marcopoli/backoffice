import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngelsComponent } from './angels.component';
import { AngelEditComponent } from './angel-edit/angel-edit.component';

const routes: Routes = [
    { path: '', component: AngelsComponent },
    { path: 'create', component: AngelEditComponent },
    { path: 'edit/:id', component: AngelEditComponent },
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AngelsRoutingModule { }
