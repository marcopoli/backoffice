import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Angel } from 'src/assets/models/angel.model';
import { Localization } from 'src/assets/models/utility/localization.model';
import { AngelStorageService } from 'src/app/services/data-storage-services/angel-storage.service';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-angel-edit',
  templateUrl: './angel-edit.component.html',
  styleUrls: ['./angel-edit.component.css'],
})
export class AngelEditComponent implements OnInit, OnDestroy {
  /**
   * Angel element to be created/modified
   *
   * @type {Angel}
   * @memberof AngelEditComponent
   */
  angel: Angel;
  /**
   * Identifier of the angel to be modified
   *
   * @type {string}
   * @memberof AngelEditComponent
   */
  id: string;
  /**
   * Define if it is a creation or an editing
   *
   * @type {boolean}
   * @memberof AngelEditComponent
   */
  isEditing: boolean;
  /**
   * Form with angel data
   *
   * @type {FormGroup}
   * @memberof AngelEditComponent
   */
  form: FormGroup;
  authDataForm: FormGroup;
  /**
   * Variable holding localized description of the angel
   *
   * @type {Localization[]}
   * @memberof AngelEditComponent
   */
  descriptions: Localization[];
  /**
   * Variable holding the name of image to be uploaded
   *
   * @type {File}
   * @memberof AngelEditComponent
   */
  imageFile: string;
  /**
   * Variable holding the images to be uploaded
   *
   * @type {FileList}
   * @memberof AngelEditComponent
   */
  uploadedFile: File;

  hasDescriptions: boolean;
  canSave: boolean;
  subscriptions: Subscription[];

  datePicker;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private angelStorageService: AngelStorageService,
    private imageManager: ImageManageService,
    private datePickerService: DatePickerService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.isEditing = !!this.id;
      this.subscriptions = [];
      this.initDatePicker();
      this.buildForms();
      this.canSave = false;
      this.isEditing ? this.getAngel() : this.initData();
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  goBack() {
    this.router.navigateByUrl('angels');
  }

  retrieveFile(event) {
    this.uploadedFile = event.target.files.item(0);
    this.imageFile = this.uploadedFile.name;
    this.verifyCanSave();
  }

  onRemoveImage() {
    this.uploadedFile = undefined;
    this.imageFile = '';
    this.verifyCanSave();
  }

  onSubmit() {
    const angel: Angel = this.form.value;
    angel.authData = this.authDataForm.value;
    angel.description = this.descriptions;

    const angelImage = _.get(this.angel, 'imageUrl', '');

    // Upload profile image first
    const subscription = this.imageManager
      .manageImageFile(angelImage, this.uploadedFile, 'guests')
      .subscribe(
        (data: { message: string; imageSrc: string }) => {
          angel.imageUrl = data.imageSrc;
          if (data.message) {
            this.commonUtilityService.showMessage(data.message);
          }
        },
        (error) => {
          const message = 'Image upload: ';
          this.commonUtilityService.showMessage(message + error);
        }
      )
      .add(() => {
        this.completeUpload(angel);
      });

    this.subscriptions.push(subscription);
  }

  private completeUpload(angel: Angel) {
    this.isEditing ? this.updateAngel(angel) : this.createAngel(angel);
  }

  private getAngel() {
    const subscription = this.angelStorageService.readAngel(this.id).subscribe(
      (data: Angel) => {
        this.angel = data;
        this.uploadedFile =
            this.angel.imageUrl === ''
              ? undefined
              : new File([], this.getFileName(this.angel.imageUrl));
        this.patchValues();
      },
      (httpError: HttpErrorResponse) => {
        this.goBack();
        this.commonUtilityService.showMessage(httpError.error.message);
      }
    );
    this.subscriptions.push(subscription);
  }

  private getFileName(imageSrc: string) {
    return _.last(imageSrc.split('/'));
  }

  private updateAngel(angel: Angel) {
    const subscription = this.angelStorageService
      .updateAngel(angel, this.id)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.goBack();
        },
        (httpError: HttpErrorResponse) =>
          this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  private createAngel(angel: Angel) {
    const subscription = this.angelStorageService.createAngel(angel).subscribe(
      (response: { status: number; data: string }) => {
        const message =
          response.status === 201
            ? 'Angel created'
            : 'Guest with same email exists. Upgraded to angel';
        this.commonUtilityService.showMessage(message);
        this.goBack();
      },
      (httpError: HttpErrorResponse) => {
        const message =
          httpError.status === 406
            ? 'Guest with same email exists and is already an angel. Creation stopped'
            : httpError.status === 400
            ? 'Soma data are not acceptable. Angel not created'
            : 'Something goes wrong. Angel not created';
        this.commonUtilityService.showMessage(message);
        if (httpError.status === 406) {
          this.goBack();
        }
      }
    );

    this.subscriptions.push(subscription);
  }

  /**
   * Date picker initialization
   *
   * @private
   * @memberof AngelEditComponent
   */
  private initDatePicker() {
    this.datePicker = this.datePickerService.initializeDatePicker(
      '#date',
      new Date(1940, 0, 1),
      new Date()
    );
    const subscripton = this.datePickerService
      .registerForChanges(this.datePicker)
      .subscribe((date) => {
        this.form.patchValue({ birthDate: date });
        this.verifyCanSave();
      });
    this.subscriptions.push(subscripton);
  }

  /**
   * Build form and initialize it
   *
   * @private
   * @memberof AngelEditComponent
   */
  private buildForms() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      birthDate: [''],
      phoneNumber: ['', Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
    });

    this.authDataForm = this.formBuilder.group({
      email: [
        { value: '', disabled: this.isEditing },
        Validators.compose([
          Validators.required,
          Validators.maxLength(70),
          Validators.pattern(
            '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'
          ),
        ]),
      ],
      password: [
        { value: '', disabled: this.isEditing },
        Validators.compose([Validators.required, Validators.minLength(7)]),
      ],
    });
  }

  /**
   * Form initialization if the page is in edit mode
   *
   * @private
   * @memberof AngelEditComponent
   */
  private patchValues() {
    this.form.patchValue(this.angel);
    this.authDataForm.patchValue(this.angel.authData);
    this.imageFile =
      this.angel.imageUrl !== ''
        ? this.imageManager.getFileName(this.angel.imageUrl)
        : '';
    this.descriptions = this.angel.description;
    this.verifyCanSave();
  }

  private initData() {
    this.uploadedFile = undefined;
    this.imageFile = '';
    this.descriptions = [
      {
        locale: 'it',
        shortDesc: '',
        longDesc: '',
      },
      {
        locale: 'en',
        shortDesc: '',
        longDesc: '',
      },
    ];

    this.verifyCanSave();
  }

  /**
   * Update description array variable with ones typed by user
   *
   * @param {*} event
   * @param {number} index
   * @memberof AngelEditComponent
   */
  updateDescription(event, index: number) {
    this.descriptions[index].longDesc = event.target.value;
    this.verifyCanSave();
  }

  verifyCanSave() {
    // In editing mode authDataForm is disabled and will be always invalid, but it is actually valid
    const authDataFormValid = this.isEditing ? true : this.authDataForm.valid;

    const descriptionsPopulated = this.descriptions.filter(
      (localized) => localized.longDesc !== ''
    );
    this.hasDescriptions = descriptionsPopulated.length > 0;
    this.canSave = this.form.valid && authDataFormValid && this.hasDescriptions;
  }
}
