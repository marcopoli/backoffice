import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngelEditComponent } from './angel-edit.component';

describe('AngelEditComponent', () => {
  let component: AngelEditComponent;
  let fixture: ComponentFixture<AngelEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngelEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
