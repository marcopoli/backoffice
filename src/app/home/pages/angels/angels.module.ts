import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AngelsRoutingModule } from './angels-routing.module';
import { DataStorageModule } from 'src/app/services/data-storage-services/data-storage.module';
import { AngelsComponent } from './angels.component';
import { AngelEditComponent } from './angel-edit/angel-edit.component';
import { ImageManageService } from 'src/app/services/utility/image-manage.service';
import { DatePickerService } from 'src/app/services/utility/date-picker.service';

@NgModule({
  declarations: [
    AngelsComponent,
    AngelEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AngelsRoutingModule,
    DataStorageModule
  ],
  providers: [
    ImageManageService,
    DatePickerService
  ],
  bootstrap: []
})
export class AngelsModule { }
