import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Angel } from 'src/assets/models/angel.model';
import { AngelStorageService } from 'src/app/services/data-storage-services/angel-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';

@Component({
  selector: 'app-angels',
  templateUrl: './angels.component.html',
  styleUrls: ['./angels.component.css']
})
export class AngelsComponent implements OnInit, OnDestroy {

  @ViewChild('checkboxSelection', { static: true }) checkboxSelection: ElementRef;

  angels: Angel[];
  subscriptions: Subscription[];
  allSelected: boolean;

  constructor(
    private router: Router,
    private angelStorageService: AngelStorageService,
    private commonUtilityService: CommonUtilityService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onDeleteAll() {
    const subscription = this.angelStorageService.deleteAngels()
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.init();
        },
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  onDelete(index: number) {
    const id = this.angels[index]._id;
    const subscription = this.angelStorageService.deleteAngel(id)
      .subscribe(
        (data: string) => {
          this.commonUtilityService.showMessage(data);
          this.init();
        },
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  onModify(index: number) {
    const id = this.angels[index]._id;
    this.router.navigateByUrl('/angels/edit/' + id);
  }

  onCreate() {
    this.router.navigateByUrl('/angels/create');
  }

  onUpdate() {
    this.init();
  }

  /**
   * Set all angel checkboxes 'checked' status to the same as the overall input checkbox
   *
   * @memberof AngelsComponent
   */
  onSelectAll() {
    this.allSelected = this.checkboxSelection.nativeElement.checked;
    this.angels.forEach((angel: Angel, index: number) => {
      const inputElement = document.getElementById('checkbox' + index) as HTMLInputElement;
      inputElement.checked = this.allSelected;
    });
  }

  private init() {
    this.angels = [];
    this.subscriptions = [];
    this.initCheckbox();
    this.getAngels();
  }

  private initCheckbox() {
    this.checkboxSelection.nativeElement.checked = false;
    this.allSelected = false;
  }

  private getAngels() {
    const subscription = this.angelStorageService.readAngels()
      .subscribe(
        (data: Angel[]) => this.angels = data,
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

}
