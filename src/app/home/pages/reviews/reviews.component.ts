import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Review } from 'src/assets/models/review.model';
import { ReviewService } from './review.service';
import { ReviewStorageService } from 'src/app/services/data-storage-services/review-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
})
export class ReviewsComponent implements OnInit, OnDestroy {
  @ViewChild('checkboxSelection', { static: true })
  checkboxSelection: ElementRef;

  reviews: Review[];
  reviewsVo: ReviewVo[];
  subscriptions: Subscription[];
  allSelected: boolean;

  constructor(
    private router: Router,
    private reviewService: ReviewService,
    private reviewStorageService: ReviewStorageService,
    private commonUtilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onDeleteAll() {
    const subscription = this.reviewStorageService.deleteReviews().subscribe(
      (data: string) => {
        this.commonUtilityService.showMessage(data);
        this.init();
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  onDelete(index: number) {
    const id = this.reviews[index]._id;
    const subscription = this.reviewStorageService.deleteReview(id).subscribe(
      (data: string) => {
        this.commonUtilityService.showMessage(data);
        this.init();
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  onViewDetail(index: number) {
    const id = this.reviews[index]._id;
    this.router.navigateByUrl('/reviews/view/' + id);
  }

  onUpdate() {
    this.init();
  }

  onSelectAll() {
    this.allSelected = this.checkboxSelection.nativeElement.checked;
    this.reviews.forEach((review: Review, index: number) => {
      const inputElement = document.getElementById(
        'checkbox' + index
      ) as HTMLInputElement;
      inputElement.checked = this.allSelected;
    });
  }

  private init() {
    this.reviews = [];
    this.subscriptions = [];
    this.initCheckbox();
    this.getReviews();
  }

  private initCheckbox() {
    this.checkboxSelection.nativeElement.checked = false;
    this.allSelected = false;
  }

  private getReviews() {
    const subscription = this.reviewStorageService.readReviews().subscribe(
      (data: Review[]) => {
        this.reviews = data;
        this.createReviewsVo();
      },
      (httpError: HttpErrorResponse) =>
        this.commonUtilityService.showMessage(httpError.error.message)
    );

    this.subscriptions.push(subscription);
  }

  private async createReviewsVo() {
    this.reviewsVo = [];

    this.reviews.forEach(async (review) => {
      const reviewVo: ReviewVo = {
        id: review._id,
        guestName: '',
        experienceName: '',
        rating: Array(review.rating).fill('star'),
      };

      // init user
      try {
        const asyncUser = await this.reviewService.getAsyncUsername(
          review.guestId
        );
        reviewVo.guestName = asyncUser;
      } catch (error) {
        reviewVo.guestName = 'Utente non trovato';
      }

      // init experience
      try {
        const asyncExperienceTitle = await this.reviewService.getAsyncExperienceTitle(
          review.microexperienceId
        );
        reviewVo.experienceName = asyncExperienceTitle;
      } catch (error) {
        reviewVo.experienceName = 'Esperienza non trovata';
      }

      this.reviewsVo.push(reviewVo);
    });
  }
}

interface ReviewVo {
  id: string;
  guestName: string;
  experienceName: string;
  rating: string[];
}
