import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewsComponent } from './reviews.component';
import { ReviewDetailComponent } from './review-detail/review-detail.component';

const routes: Routes = [
    { path: '', component: ReviewsComponent },
    { path: 'view/:id', component: ReviewDetailComponent },
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReviewsRoutingModule { }