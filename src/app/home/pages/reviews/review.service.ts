import { Injectable } from '@angular/core';
import { GuestStorageService } from 'src/app/services/data-storage-services/guest-storage.service';
import { MicroexperienceStorageService } from 'src/app/services/data-storage-services/microexperience-storage.service';
import { Guest } from 'src/assets/models/guest.model';
import { Microexperience } from 'src/assets/models/microexperience.model';

@Injectable()
export class ReviewService {

    constructor(
        private guestStorageService: GuestStorageService,
        private microexperienceStorageService: MicroexperienceStorageService
    ) {}

    getAsyncUsername(guestId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.guestStorageService.readGuest(guestId).subscribe(
                (guest: Guest) => {
                    guest.name === '' && guest.surname === '' ?
                        resolve('Anonimo') :
                        resolve(guest.name + ' ' + guest.surname);
                },
                (error) => reject()
            );
        });
    }

    getAsyncExperienceTitle(experienceId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.microexperienceStorageService.readMicroexperience(experienceId).subscribe(
                (experience: Microexperience) => resolve(experience.basicInfo.title),
                (error) => reject()
            );
        });
    }

}