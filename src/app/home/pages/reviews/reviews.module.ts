import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ReviewsRoutingModule } from './reviews-routing.module';
import { DataStorageModule } from 'src/app/services/data-storage-services/data-storage.module';
import { ReviewsComponent } from './reviews.component';
import { ReviewDetailComponent } from './review-detail/review-detail.component';
import { ReviewService } from './review.service';

@NgModule({
  declarations: [
    ReviewsComponent,
    ReviewDetailComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ReviewsRoutingModule,
    DataStorageModule
  ],
  providers: [
    ReviewService
  ],
  bootstrap: []
})
export class ReviewsModule { }
