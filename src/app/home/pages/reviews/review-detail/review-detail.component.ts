import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Review } from 'src/assets/models/review.model';
import { ReviewService } from '../review.service';
import { ReviewStorageService } from 'src/app/services/data-storage-services/review-storage.service';
import { CommonUtilityService } from 'src/app/services/utility/common-utility.service';

@Component({
  selector: 'app-review-detail',
  templateUrl: './review-detail.component.html',
  styleUrls: ['./review-detail.component.css']
})
export class ReviewDetailComponent implements OnInit, OnDestroy {

  review: Review;
  id: string;
  experienceName: string;
  guestName: string;
  rating: string[];
  subscriptions: Subscription[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private reviewService: ReviewService,
    private reviewStorageService: ReviewStorageService,
    private commonUtilityService: CommonUtilityService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.subscriptions = [];
      this.getReview();
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  goBack() {
    this.router.navigateByUrl('/reviews');
  }

  private getReview() {
    const subscription = this.reviewStorageService.readReview(this.id)
      .subscribe(
        (data: Review) => {
          this.review = data;
          this.initData();
        },
        (httpError: HttpErrorResponse) => this.commonUtilityService.showMessage(httpError.error.message)
      );

    this.subscriptions.push(subscription);
  }

  private async initData() {
    this.rating = Array(this.review.rating).fill('star');

    // init user
    try {
      const asyncUser = (await this.reviewService.getAsyncUsername(this.review.guestId));
      this.guestName = asyncUser;
    } catch (error) {
      this.guestName = 'Utente non trovato';
    }

    // init experience
    try {
      const asyncExperienceTitle = (await this.reviewService.getAsyncExperienceTitle(this.review.microexperienceId));
      this.experienceName = asyncExperienceTitle;
    } catch (error) {
      this.experienceName = 'Esperienza non trovata';
    }
  }

}
