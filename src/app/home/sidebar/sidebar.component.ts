import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { SidebarService } from '../../services/utility/sidebar.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  isSidebarOpened: boolean;
  subscription: Subscription;
  activeLink: ActiveLink = {
      angels: false,
      guests: false,
      microexperiences: false,
      reviews: false
  };

  constructor(
    private router: Router,
    private sidebarService: SidebarService
  ) { }

  ngOnInit() {
    this.isSidebarOpened = this.sidebarService.getIsSidebarOpened();
    this.subscription = this.sidebarService.isSidebarToogled.subscribe(() => this.isSidebarOpened = !this.isSidebarOpened);
    this.initActiveLinks(this.router.url);
    this.trackNavigation();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onNavigate() {
    this.sidebarService.setIsSidebarOpened();
  }

  /**
   * Listen on Angular Router object to get navigated url
   *
   * @private
   * @memberof SidebarComponent
   */
  private trackNavigation() {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.initActiveLinks(event.url);
      }
    });
  }

  /**
   * Keeps track of the visited url to update sidebar active element accordingly
   *
   * @private
   * @param {string} url
   * url to match label with
   * @memberof SidebarComponent
   */
  private initActiveLinks(url: string) {
    const keyToCompare = url.split('/')[1];

    Object.keys(this.activeLink).forEach((key) => {
      this.activeLink[key] = key === keyToCompare;
    });
  }

}

interface ActiveLink {
  angels: boolean;
  guests: boolean;
  microexperiences: boolean;
  reviews: boolean;
}
