import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
    { path: '', component: HomeComponent,
        children: [
        { path: 'angels', loadChildren: () => import('./pages/angels/angels.module').then(m => m.AngelsModule) },
        { path: 'guests', loadChildren: () => import('./pages/guests/guests.module').then(m => m.GuestsModule) },
        { path: 'microexperiences', loadChildren: () => import('./pages/microexperiences/microexperiences.module').then(m => m.MicroexperiencesModule) },
        { path: 'reviews', loadChildren: () => import('./pages/reviews/reviews.module').then(m => m.ReviewsModule) },
        { path: '**', redirectTo: 'angels'}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
