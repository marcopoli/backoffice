import { Injectable } from '@angular/core';
import { Guest } from 'src/assets/models/guest.model';

@Injectable()
export class LocalStorageManager {

    private USERNAME = 'username';

    constructor() {}

    /**
     * Save the admin username into local storage
     *
     * @param {Guest} user
     * @memberof LocalStorageManager
     */
    setUsername(user: Guest) {
        localStorage.setItem(this.USERNAME, JSON.stringify(user.name + ' ' + user.surname));
    }

    /**
     * Get the admin username form local storage
     *
     * @returns {string}
     * @memberof LocalStorageManager
     */
    getUsername(): string {
        return JSON.parse(localStorage.getItem(this.USERNAME));
    }

    /**
     * Remove the admin username from local storage
     *
     * @memberof LocalStorageManager
     */
    removeUsername() {
        localStorage.removeItem(this.USERNAME);
    }

}
