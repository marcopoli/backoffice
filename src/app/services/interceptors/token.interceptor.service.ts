import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TokenManagerService } from '../token-manager.service';
import * as _ from 'lodash';

/**
 * Incercept any HTTP response to check if they have a token.
 * If a token is found, it will be saved into local storage.
 *
 * @export
 * @class TokenInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private tokenManagerService: TokenManagerService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get request to be executed and read response
        return next.handle(req).pipe(tap(
            (event: HttpEvent<any>) => {
                // catch the response
                if (event instanceof HttpResponse) {
                    const responseBody: any = event.body;

                    if (this.tokenManagerService.isTokenData(responseBody) || _.has(responseBody, 'token')) {
                        // Response got a token data, save it
                        const token = _.get(responseBody, 'token', responseBody);
                        this.tokenManagerService.setTokenData(token);
                    }
                }
            },
            (error: any) => {
                if (error instanceof HttpErrorResponse) {
                    // something went wrong, log to console
                    console.error('Something went wrong with request %o', req);
                    console.error('Here is the error', error);
                }
            }
        ));
    }

}
