import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenManagerService } from '../token-manager.service';

/**
 * Intercept any outgoing HTTP request and append the JWT token (if exists) to it.
 * The token will be appended only for routes that will require it.
 *
 * @export
 * @class AppendAuthenticationInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class AppendAuthenticationInterceptor implements HttpInterceptor {

    private AUTH_HEADER = 'Authorization';
    private AUTH_TYPE = 'Bearer ';
    private excludedRoute = '/login/admin';

    constructor(
        private tokenManagerService: TokenManagerService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.url.includes(this.excludedRoute)) {
            req = this.appendToken(req);
        }
        return next.handle(req);
    }

    /**
     * Retrieve token and set it
     *
     * @private
     * @param {HttpRequest<any>} request
     * @returns {HttpRequest<any>}
     * @memberof AppendAuthenticationInterceptor
     */
    private appendToken(request: HttpRequest<any>): HttpRequest<any> {
        const token = this.tokenManagerService.getToken();
        return !token ? request : request.clone({ headers: request.headers.set(this.AUTH_HEADER, this.AUTH_TYPE + token) });
    }

}
