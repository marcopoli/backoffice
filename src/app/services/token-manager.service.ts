import { Injectable } from '@angular/core';

@Injectable()
export class TokenManagerService {
  private TOKEN_DATA = 'tokenData';

  constructor() {}

  /**
   * Retrieve token data from the local storage
   *
   * @private
   * @returns {TokenData}
   * @memberof LocalStorageService
   */
  private getTokenData(): TokenData {
    const tokenData = localStorage.getItem(this.TOKEN_DATA);
    return tokenData ? JSON.parse(tokenData) : undefined;
  }

  /**
   * Save token data into the local storage
   *
   * @param {*} tokenData
   * @memberof LocalStorageService
   */
  setTokenData(tokenData) {
    localStorage.setItem(this.TOKEN_DATA, JSON.stringify(tokenData));
  }

  /**
   * Remove token data from the local storage
   *
   * @memberof TokenManagerService
   */
  destroyTokenData() {
    localStorage.removeItem(this.TOKEN_DATA);
  }

  /**
   * Retrieve token from local storage
   *
   * @returns
   * @memberof LocalStorageService
   */
  getToken() {
    // TODO: When token expiration will be added, change this function
    return this.getTokenData();
  }

  /**
   * Check if the given object is a token data
   *
   * @param {*} responseObject
   * @returns {boolean}
   * @memberof TokenManagerService
   */
  isTokenData(responseObject: any): boolean {
    return (
      responseObject &&
      typeof responseObject === 'object' &&
      'token' in responseObject
    );
  }
}

export interface TokenData {
  token: string;
}
