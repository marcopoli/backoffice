import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenManagerService } from '../token-manager.service';
/**
 * Guard to prevent already logged user to access to authentication page
 *
 * @export
 * @class LoginBlockGuard
 * @implements {CanActivate}
 */
@Injectable({providedIn: 'root'})
export class LoginBlockGuard implements CanActivate {

  constructor(
    private router: Router,
    private tokenManager: TokenManagerService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const token = this.tokenManager.getToken();

    if (token) {
        // already logged in
        this.router.navigateByUrl('');
        return false;
    } else {
        // not logged in
        return true;
    }
  }

}
