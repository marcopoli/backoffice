import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TokenManagerService } from '../token-manager.service';
import { AuthenticationService } from '../data-storage-services/authentication.service';
import { LocalStorageManager } from '../local-storage-manager.service';

/**
 * Guard to prevent unwanted user to access protected routes
 *
 * @export
 * @class AuthGuard
 * @implements {CanActivate}
 */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private tokenManager: TokenManagerService,
    private authenticationService: AuthenticationService,
    private localStorageManager: LocalStorageManager
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    const token = this.tokenManager.getToken();

    if (token) {
      return new Observable((observer) => {
        this.authenticationService.verifyToken(token).subscribe(
          () => observer.next(true),
          () => {
            this.tokenManager.destroyTokenData();
            this.localStorageManager.removeUsername();
            this.router.navigateByUrl('/auth');
            observer.next(false);
          }
        );
      });
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigateByUrl('/auth');
      return false;
    }
  }
}
