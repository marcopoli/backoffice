import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MediaService } from '../data-storage-services/media.service';
import * as _ from 'lodash';

@Injectable()
export class ImageManageService {

    constructor(private mediaService: MediaService) {}

    /**
     * Return the name of the image from a path
     *
     * @param {string} imageSrc
     * @returns
     * @memberof ImageManageService
     */
    getFileName(imageSrc: string) {
        return _.last(imageSrc.split('/'));
    }

    /**
     * Manage the upload process of a single image
     *
     * @param {string} oldImage
     * @param {File} file
     * @param {('guests' | 'microexperiences')} resourceType
     * @returns
     * @memberof ImageManageService
     */
    manageImageFile(oldImage: string, file: File, resourceType: 'guests' | 'microexperiences'): Observable<ImageFileFeedback> {
        return new Observable(observer => {
            const oldFilename = this.getFileName(oldImage);
            const isFileUnchanged = _.get(file, 'name') === oldFilename;
            if (isFileUnchanged) {
                observer.next({
                    message: undefined,
                    imageSrc: oldImage
                });
                observer.complete();
            }

            const hasNoImageAtAll = oldImage === '' && file === undefined;
            if (hasNoImageAtAll) {
                observer.next({
                    message: undefined,
                    imageSrc: ''
                });
                observer.complete();
            }

            const mustUploadForFirstTime = oldImage === '' && file;
            if (mustUploadForFirstTime) {
                this.upload(resourceType, file).subscribe(
                    (value) => {
                        observer.next(value);
                        observer.complete();
                    },
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            }

            const mustDeletePreviousImage = oldImage !== '' && file === undefined;
            if (mustDeletePreviousImage) {
                this.mediaService.deleteImage(resourceType, oldFilename);
                observer.next({
                    message: undefined,
                    imageSrc: ''
                });
                observer.complete();
            }

            const hasChangedFile = oldImage !== '' && file && file.name !== oldFilename;
            if (hasChangedFile) {
                this.mediaService.deleteImage(resourceType, oldFilename);
                this.upload(resourceType, file).subscribe(
                    (value) => observer.next(value),
                    (error) => observer.error(error)
                )
                .add(() => observer.complete());
            }
        });
    }

    private upload(resourceType: 'guests' | 'microexperiences', file: File): Observable<ImageFileFeedback> {
        return new Observable(observer => {
            switch (resourceType) {
                case 'microexperiences':
                    // upload main image for experiences
                    this.mediaService.uploadMainImage(file)
                    .subscribe(
                        (imageSrc: string) => {
                            observer.next({
                                message: 'Image uploaded successfully',
                                imageSrc: imageSrc
                            });
                        },
                        (httpError: HttpErrorResponse) => observer.error(httpError.error.message)
                    )
                    .add(() => observer.complete());
                    break;

                default:
                    // upload image for guests
                    this.mediaService.uploadProfileImage(file)
                        .subscribe(
                            (imageSrc: string) => {
                                observer.next({
                                    message: 'Image uploaded successfully',
                                    imageSrc: imageSrc
                                });
                            },
                            (httpError: HttpErrorResponse) => observer.error(httpError.error.message)
                        )
                        .add(() => observer.complete());
                    break;
            }
        });
    }

    /**
     * Manage the upload process of gallery images
     *
     * @param {string[]} oldGallery
     * @param {File[]} newGallery
     * @returns
     * @memberof ImageManageService
     */
    manageGalleryFiles(oldGallery: string[], newGallery: File[]) {
        // remove added and unused files
        newGallery = newGallery.filter((image: File) => {
            return image !== undefined;
        });

        // remove deleted images from oldGallery
        const galleryNames = newGallery.map(image => image.name);

        oldGallery = oldGallery.filter(image => {
            const isNotDeleted = galleryNames.includes(this.getFileName(image));
            if (!isNotDeleted) {
                // remove image remotely
                this.mediaService.deleteGalleryImage('microexperiences', this.getFileName(image));
            }

            return isNotDeleted;
        });

        // remove from gallery the already existing files
        newGallery = newGallery.filter(galleryImage => {
            const existingGallery = oldGallery.map(image => this.getFileName(image));
            return !existingGallery.includes(galleryImage.name);
        });

        return new Observable(observer => {
            if (newGallery.length === 0) {
                // no images to upload
                observer.next({
                    message: undefined,
                    uploadedGallery: oldGallery
                })
                observer.complete();
            } else {
                this.mediaService.uploadGalleryImages(newGallery)
                    .subscribe(
                        (data: { uploaded: string[], errors: string[] }) => {
                            data.uploaded.forEach(src => oldGallery.push(src));
                            let message = 'All gallery images uploaded';
                            if (data.errors.length > 0) {
                                message = data.errors.length + ' gallery images not uploaded';
                            }
                            observer.next({
                                message: message,
                                uploadedGallery: oldGallery
                            });
                            observer.complete();
                        },
                        (httpError: HttpErrorResponse) => {
                            const message = 'Gallery upload: ' + httpError.error.message;
                            observer.error(message);
                            observer.complete();
                        }
                    );
            }
        });
    }

}

interface ImageFileFeedback {
    message: string;
    imageSrc: string;
}
