import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { DateService } from './date.service';
import TinyDatePicker from 'tiny-date-picker';
import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable()
export class DatePickerService {
  /**
   * Emitter to return selected date to the component
   *
   * @type {EventEmitter<string>}
   * @memberof DatePickerService
   */
  obtainedDate: EventEmitter<string> = new EventEmitter();

  /**
   * Option to localize the datepicker to italian
   *
   * @memberof DatePickerService
   */
  lang = {
    days: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
    months: [
      'Gennaio',
      'Febbraio',
      'Marzo',
      'Aprile',
      'Maggio',
      'Giugno',
      'Luglio',
      'Agosto',
      'Settembre',
      'Ottobre',
      'Novembre',
      'Dicembre',
    ],
    today: 'Oggi',
    clear: 'Cancella',
    close: 'Chiudi',
  };

  constructor(private dateService: DateService) {}

  /**
   * Initialize the TinyDatePicker, setting it in italian, starting week from monday and as a modal
   *
   * @param {string} id
   * Html element id to append the date picker
   * @param {Date} maxDate
   * Min date limit that can be selected
   * @memberof DatePickerService
   * Max date limit that can be selected
   * @param {Date} minDate
   */
  initializeDatePicker(id: string, minDate: Date, maxDate?: Date) {
    const datePicker = TinyDatePicker(id, {
      format: (date: Date) => this.dateService.formatDate(date),
      parse: function (str) {
        const date = new Date(str);
        return date ? new Date() : date;
      },
      mode: 'dp-modal',
      lang: this.lang,
      dayOffset: 1,
      max: maxDate ? maxDate : moment(maxDate).add(5, 'year').toDate(),
      min: minDate,
    });

    return datePicker;
  }

  registerForChanges(datePicker) {
    return new Observable((observer) => {
      datePicker.on('statechange', (_event, picker) => {
        const date: Date = _.get(picker, 'state.selectedDate');
        const formatted = date ? this.formatDate(date) : '';
        observer.next(formatted);
      });
    });
  }

  private formatDate(date: Date) {
    return this.dateService.formatDate(date);
  }
}
