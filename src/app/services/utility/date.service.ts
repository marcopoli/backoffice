import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class DateService {
  CUSTOM_DATE_FORMAT = 'DD/MM/YYYY';
  CUSTOM_HOUR_FORMAT = 'HH:mm';
  CUSTOM_DATE_HOUR_FORMAT = 'DD/MM/YYYY - HH:mm';

  constructor() {}

  /**
   * Return the Date object in a string formatted as "day/month/year"
   *
   * @param {Date} date
   * @returns
   * @memberof DateService
   */
  formatDate(date: Date) {
    return moment(date).format(this.CUSTOM_DATE_FORMAT);
  }

  /**
   * Return the Date object in a string formatted as "hour:min"
   *
   * @param {Date} date
   * @returns
   * @memberof DateService
   */
  formatHour(date: Date) {
    return moment(date).format(this.CUSTOM_HOUR_FORMAT);
  }

  /**
   * Parse a string with an hour format
   *
   * @param {string} date
   * @returns
   * @memberof DateService
   */
  parseHourWithValidation(date: string) {
    return moment(date, this.CUSTOM_HOUR_FORMAT).toDate();
  }

  /**
   * Parse a string with a date-hour format
   *
   * @param {Date} date
   * @returns
   * @memberof DateService
   */
  parseDateHourWithValidation(date: string) {
    return moment(date, this.CUSTOM_DATE_HOUR_FORMAT).toDate();
  }

  formatDateUTC(date: Date) {
    return moment.utc(date).format(this.CUSTOM_DATE_FORMAT);
  }

  formatHourUTC(date: Date) {
    return moment.utc(date).format(this.CUSTOM_HOUR_FORMAT);
  }

  formatDateHourUTC(date: Date) {
    return moment.utc(date).format(this.CUSTOM_DATE_HOUR_FORMAT);
  }

  parseDateHourWithValidationUTC(date: string) {
    return moment.utc(date, this.CUSTOM_DATE_HOUR_FORMAT).toDate();
  }

  convertDateToUTC(date: Date) {
    return new Date(
      Date.UTC(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes()
      )
    );
  }

  convertStringToUTC(date: string) {
    return moment(date).toDate();
  }
}
