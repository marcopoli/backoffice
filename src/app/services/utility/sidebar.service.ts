import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class SidebarService {

    /**
     * Flag to collapse sidebar
     *
     * @private
     * @type {boolean}
     * @memberof SidebarService
     */
    private isSidebarOpened = false;

    /**
     * Event emitter for the collapse functionality
     *
     * @memberof SidebarService
     */
    isSidebarToogled = new EventEmitter();

    constructor() {
    }

    /**
     * Return sidebar status
     *
     * @returns {boolean}
     * @memberof SidebarService
     */
    getIsSidebarOpened(): boolean {
        return this.isSidebarOpened;
    }

    /**
     * Toogle sidebar status
     *
     * @memberof SidebarService
     */
    setIsSidebarOpened() {
        this.isSidebarOpened = !this.isSidebarOpened;
        this.isSidebarToogled.emit();
    }
}
