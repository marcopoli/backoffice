import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class CommonUtilityService {

    constructor() {}

    /**
     * Show alert message
     *
     * @param {string} message
     * @memberof CommonUtilityService
     */
    showMessage(message: string) {
        alert(message);
    }

}
