import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ItineraryOptions } from 'src/assets/models/itinerary.model';

@Injectable()
export class ItineraryStorageService {
  private itineraryPath = environment.host + '/itineraries';

  constructor(private http: HttpClient) {}

  /**
   * Download itineraries data
   *
   * @returns {Observable<Object>}
   * @memberof ItineraryStorageService
   */
  readItineraries(): Observable<Object> {
    return this.http.get(this.itineraryPath);
  }

  /**
   * Download a specific itinerary
   *
   * @param {string} id
   * @returns {Observable<Object>}
   * @memberof ItineraryStorageService
   */
  readItinerary(id: string): Observable<Object> {
    return this.http.get(this.itineraryPath + '/' + id);
  }

  /**
   * Create a new itinerary from a filtered search on experiences
   *
   * @param {string} categoryId
   * @param {string} day
   * @param {number} timeAvailable
   * @returns {Observable<Object>}
   * @memberof ItineraryStorageService
   */
  createItinerary(options: ItineraryOptions): Observable<Object> {
    return this.http.post(this.itineraryPath, options);
  }

  /**
   * Save/Unsave the itinerary for the logged user
   *
   * @param {string} itineraryId
   * Id of the itinerary to be saved
   * @param {string} name
   * Name to assign to the itinerary
   * @returns
   * @memberof ItineraryStorageService
   */
  saveItinerary(itineraryId: string, name: string) {
    return this.http.post(this.itineraryPath + '/' + itineraryId + '/save', {
      name,
    });
  }

  /**
   * Get all itineraries saved by a specific guest
   *
   * @param {string} guestId
   * @returns
   * @memberof ItineraryStorageService
   */
  getGuestSavedItineraries(guestId: string) {
    return this.http.get(this.itineraryPath + '/' + guestId + '/saved');
  }

  /**
   * Clone an itinerary shared from another user
   *
   * @param {string} itineraryId
   * @returns
   * @memberof ItineraryStorageService
   */
  cloneItinerary(itineraryId: string) {
    return this.http.post(
      this.itineraryPath + '/' + itineraryId + '/clone',
      {}
    );
  }
}
