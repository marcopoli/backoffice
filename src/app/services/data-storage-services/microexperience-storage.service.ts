import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Microexperience } from 'src/assets/models/microexperience.model';
import { ItineraryOptions } from 'src/assets/models/itinerary.model';

@Injectable()
export class MicroexperienceStorageService {
  private microexperiencePath = environment.host + '/microexperiences';

  constructor(private http: HttpClient) {}

  /**
   * Download microexperiences data
   *
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  readMicroexperiences(): Observable<Object> {
    return this.http.get(this.microexperiencePath);
  }

  /**
   * Delete all microexperiences data
   *
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  deleteMicroexperiences(): Observable<Object> {
    return this.http.delete(this.microexperiencePath);
  }

  /**
   * Create an microexperience
   *
   * @param {Microexperience} microexperience
   * Data of the microexperience to be created
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  createMicroexperience(microexperience: Microexperience): Observable<Object> {
    return this.http.post(this.microexperiencePath, microexperience);
  }

  /**
   * Download a specific microexperience
   *
   * @param {string} id
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  readMicroexperience(id: string): Observable<Object> {
    return this.http.get(this.microexperiencePath + '/' + id);
  }

  /**
   * Update values for a specific microexperience
   *
   * @param {Microexperience} microexperience
   * Updated microexperience model
   * @param {string} id
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  updateMicroexperience(
    microexperience: Microexperience,
    id: string
  ): Observable<Object> {
    return this.http.put(this.microexperiencePath + '/' + id, microexperience);
  }

  /**
   * Delete a specific microexperience
   *
   * @param {string} id
   * @returns {Observable<Object>}
   * @memberof MicroexperienceStorageService
   */
  deleteMicroexperience(id: string): Observable<Object> {
    return this.http.delete(this.microexperiencePath + '/' + id);
  }

  /**
   * Search the microexperiences data of the specified angel
   *
   * @param {string} angelId
   * @memberof MicroexperienceStorageService
   */
  filterExperiencesByAngelId(angelId: string) {
    return this.http.get(
      this.microexperiencePath + '?basicInfo.angelId=' + angelId
    );
  }

  filterExperienceByCategoryDayTime(itineraryOptions: ItineraryOptions) {
    const categoryParam = 'categoryId=' + itineraryOptions.categoryId;
    const startParam = '&start=' + itineraryOptions.start;
    const hoursParam = '&hoursAvailable=' + itineraryOptions.hoursAvailable;
    const queryParams = categoryParam + startParam + hoursParam;
    return this.http.get(this.microexperiencePath + '?' + queryParams);
  }
}
