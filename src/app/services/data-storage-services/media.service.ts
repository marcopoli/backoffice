import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class MediaService {

    private mediaPath = environment.host + '/media';

    constructor(private http: HttpClient) {}

    uploadProfileImage(fileToUpload: File) {
        const formData = new FormData();
        formData.append('uploads[]', fileToUpload, fileToUpload.name);

        return this.http.post(this.mediaPath + '/upload/profile-image', formData);
    }

    uploadMainImage(fileToUpload: File) {
        const formData = new FormData();
        formData.append('uploads[]', fileToUpload, fileToUpload.name);

        return this.http.post(this.mediaPath + '/upload/main-image', formData);
    }

    uploadGalleryImages(filesToUpload: File[]) {
        const formData = new FormData();
        for (let i = 0; i < filesToUpload.length; i++) {
            formData.append('uploads[]', filesToUpload[i], filesToUpload[i].name);
        }

        return this.http.post(this.mediaPath + '/upload/gallery', formData);
    }

    deleteImage(resource: string, fileName: string) {
        return this.http.delete(this.mediaPath + '/delete/image/' + resource + '/' + fileName)
            .subscribe(() => {});
    }

    deleteGalleryImage(resource: string, fileName: string) {
        return this.http.delete(this.mediaPath + '/delete/gallery/' + resource + '/' + fileName)
            .subscribe(() => {});
    }

}
