import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Guest } from 'src/assets/models/guest.model';

@Injectable()
export class GuestStorageService {

    private guestPath = environment.host + '/guests';

    constructor(private http: HttpClient) {}

    /**
     * Download guests data
     *
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    readGuests(): Observable<Object> {
        return this.http.get(this.guestPath);
    }

    /**
     * Delete all guests data
     *
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    deleteGuests(): Observable<Object> {
        return this.http.delete(this.guestPath);
    }

    /**
     * Create a guest
     *
     * @param {Guest} guest
     * Data of the guest to be created
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    createGuest(guest: Guest): Observable<Object> {
        return this.http.post(this.guestPath, guest);
    }

    /**
     * Download a specific guest
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    readGuest(id: string): Observable<Object> {
        return this.http.get(this.guestPath + '/' + id);
    }

    /**
     * Update values for a specific guest
     *
     * @param {Guest} guest
     * Updated guest data
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    updateGuest(guest: Guest, id: string): Observable<Object> {
        return this.http.put(this.guestPath + '/' + id, guest);
    }

    /**
     * Delete a specific guest
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof GuestStorageService
     */
    deleteGuest(id: string): Observable<Object> {
        return this.http.delete(this.guestPath + '/' + id);
    }

    /**
     * Search the guest data of the specified angel
     *
     * @param {string} angelId
     * @memberof GuestStorageService
     */
    filterGuestsByAngelId(angelId: string) {
        return this.http.get(this.guestPath + '?angelId=' + angelId);
    }

    /**
     * Search a guest that matches all parameters
     *
     * @param {string} surname
     * @param {string} name
     * @param {string} birthDate
     * @param {string} phoneNumber
     * @returns
     * @memberof GuestStorageService
     */
    filterGuestsByParams(surname: string, name: string, birthDate: string, phoneNumber: string) {
        const queryParams = 'surname=' + surname +
                            '&name=' + name +
                            '&birthDate=' + birthDate +
                            '&phoneNumber=' + phoneNumber;
        return this.http.get(this.guestPath + '?' + queryParams);
    }

}
