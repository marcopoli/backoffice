import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Guest } from 'src/assets/models/guest.model';

@Injectable()
export class AuthenticationService {

    private authPath = environment.host + '/auth';

    constructor(private http: HttpClient) {}

    /**
     * Create an anonymous user
     *
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    createAnonymousUser(): Observable<Object> {
        return this.http.post(this.authPath + '/anonymous', {});
    }

    /**
     * Register a user
     *
     * @param {Guest} guest
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    registerUser(guest: Guest): Observable<Object> {
        return this.http.post(this.authPath + '/register', guest);
    }

    /**
     * Login a user
     *
     * @param {string} email
     * @param {string} password
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    loginUser(email: string, password: string): Observable<Object> {
        const body = { email, password };
        return this.http.post(this.authPath + '/login', body);
    }

    /**
     * Login an administrator
     *
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    loginAdmin(email: string, password: string): Observable<Object> {
        const body = { email, password };
        return this.http.post(this.authPath + '/login/admin', body);
    }

    /**
     * Logout a user
     *
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    logoutUser(): Observable<Object> {
        return this.http.post(this.authPath + '/logout', {});
    }

    /**
     * Logout a user from all devices
     *
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    logoutUserFromAllDevices(): Observable<Object> {
        return this.http.post(this.authPath + '/logout-all', {});
    }

    /**
     * Verify if the token is still valid
     *
     * @param {*} token
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    verifyToken(token): Observable<Object> {
        return this.http.post(this.authPath + '/verify-token', { token });
    }

    /**
     * Verify if the email has already been used
     *
     * @param {string} email
     * @returns {Observable<Object>}
     * @memberof AuthenticationService
     */
    mailNotAlreadyUsed(email: string): Observable<Object> {
        return this.http.get(this.authPath + '/mail-not-used/' + email);
    }

}
