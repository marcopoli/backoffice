import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Angel } from 'src/assets/models/angel.model';

@Injectable()
export class AngelStorageService {

    private angelPath = environment.host + '/angels';

    constructor(private http: HttpClient) {}

    /**
     * Download angels data
     *
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    readAngels(): Observable<Object> {
        return this.http.get(this.angelPath);
    }

    /**
     * Delete all angels data
     *
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    deleteAngels(): Observable<Object> {
        return this.http.delete(this.angelPath);
    }

    /**
     * Create an angel
     *
     * @param {Angel} angel
     * Data of the angel to be created
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    createAngel(angel: Angel): Observable<Object> {
        return this.http.post(this.angelPath, angel);
    }

    /**
     * Download a specific angel
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    readAngel(id: string): Observable<Object> {
        return this.http.get(this.angelPath + '/' + id);
    }

    /**
     * Update values for a specific angel
     *
     * @param {Angel} angel
     * Updated angel model
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    updateAngel(angel: Angel, id: string): Observable<Object> {
        return this.http.put(this.angelPath + '/' + id, angel);
    }

    /**
     * Delete a specific angel
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof AngelStorageService
     */
    deleteAngel(id: string): Observable<Object> {
        return this.http.delete(this.angelPath + '/' + id);
    }

}
