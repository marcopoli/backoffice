import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ProfileService {

    private profilePath = environment.host + '/profile';

    constructor(private http: HttpClient) {}

    /**
     * Get the profile of the current logged in user
     *
     * @returns {Observable<Object>}
     * @memberof ProfileService
     */
    getLoggedUserProfile(): Observable<Object> {
        return this.http.get(this.profilePath);
    }

}
