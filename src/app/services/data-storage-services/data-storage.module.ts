import { NgModule } from '@angular/core';
import { AngelStorageService } from './angel-storage.service';
import { AuthenticationService } from './authentication.service';
import { CategoryStorageService } from './category-storage.service';
import { GameStorageService } from './game-storage.service';
import { GuestStorageService } from './guest-storage.service';
import { ItineraryStorageService } from './itineraries-storage.service';
import { MediaService } from './media.service';
import { MicroexperienceStorageService } from './microexperience-storage.service';
import { ProfileService } from './profile.service';
import { ReviewStorageService } from './review-storage.service';

@NgModule({
  providers: [
    AngelStorageService,
    AuthenticationService,
    CategoryStorageService,
    GameStorageService,
    GuestStorageService,
    ItineraryStorageService,
    MediaService,
    MicroexperienceStorageService,
    ProfileService,
    ReviewStorageService,
  ],
})
export class DataStorageModule {}
