import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Microexperience } from 'src/assets/models/microexperience.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GameStorageService {
  private gamePath = environment.host + '/game';

  constructor(private http: HttpClient) {}

  createGame(itineraryId: string, entities: Microexperience[]) {
    const body = { itineraryId, entities };
    return this.http.post(this.gamePath, body);
  }

  readGame(gameId: string) {
    return this.http.get(this.gamePath + '/' + gameId);
  }

  readGameFromItinerary(itineraryId: string) {
    return this.http.get(this.gamePath + '/from-itinerary/' + itineraryId);
  }

  playExperience(gameId: string, experienceId: string) {
    const body = { gameId, experienceId };
    return this.http.post(this.gamePath + '/play-experience', body);
  }
}
