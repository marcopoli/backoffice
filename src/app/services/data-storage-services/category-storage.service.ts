import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class CategoryStorageService {

    private categoryPath = environment.host + '/categories';

    constructor(private http: HttpClient) {}

    /**
     * Download categories data
     *
     * @returns {Observable<Object>}
     * @memberof CategoryStorageService
     */
    readCategories(): Observable<Object> {
        return this.http.get(this.categoryPath);
    }

    /**
     * Download a specific category
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof CategoryStorageService
     */
    readCategory(id: string): Observable<Object> {
        return this.http.get(this.categoryPath + '/' + id);
    }

}
