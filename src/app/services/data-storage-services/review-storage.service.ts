import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ReviewStorageService {

    private reviewPath = environment.host + '/reviews';

    constructor(private http: HttpClient) {}

    /**
     * Download reviews data
     *
     * @returns {Observable<Object>}
     * @memberof ReviewStorageService
     */
    readReviews(): Observable<Object> {
        return this.http.get(this.reviewPath);
    }

    /**
     * Delete all reviews data
     *
     * @returns {Observable<Object>}
     * @memberof ReviewStorageService
     */
    deleteReviews(): Observable<Object> {
        return this.http.delete(this.reviewPath);
    }

    /**
     * Download a specific review
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof ReviewStorageService
     */
    readReview(id: string): Observable<Object> {
        return this.http.get(this.reviewPath + '/' + id);
    }

    /**
     * Delete a specific review
     *
     * @param {string} id
     * @returns {Observable<Object>}
     * @memberof ReviewStorageService
     */
    deleteReview(id: string): Observable<Object> {
        return this.http.delete(this.reviewPath + '/' + id);
    }

    /**
     * Search the reviews data of the specified microexperience
     *
     * @param {string} microexperienceId
     * @memberof ReviewStorageService
     */
    filterReviewsByExperienceId(microexperienceId: string) {
        return this.http.get(this.reviewPath + '?microexperienceId=' + microexperienceId);
    }

    /**
     * Search the reviews data of the specified guestId
     *
     * @param {string} guestId
     * @memberof ReviewStorageService
     */
    filterReviewsByGuestId(guestId: string) {
        return this.http.get(this.reviewPath + '?guestId=' + guestId);
    }

}
