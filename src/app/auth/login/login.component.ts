import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/data-storage-services/authentication.service';
import { LocalStorageManager } from 'src/app/services/local-storage-manager.service';
import { Guest } from 'src/assets/models/guest.model';
import { CONSTANTS } from 'src/assets/consts/app.const';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  project = CONSTANTS.project;
  form: FormGroup;
  error: boolean;
  invalidCredential: boolean;
  showPassword: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private localStorageManager: LocalStorageManager
  ) { }

  ngOnInit() {
    this.error = false;
    this.invalidCredential = false;
    this.showPassword = false;
    this.buildForm();
  }

  onLogin() {
    const email = this.form.value.email;
    const password = this.form.value.password;

    this.authenticationService.loginAdmin(email, password)
      .subscribe(
        (data: { user: Guest }) => this.onLoginSuccess(data.user),
        (error: HttpErrorResponse) => this.handleError(error)
      );
  }

  showHidePassword() {
    this.showPassword = !this.showPassword;
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(70),
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(7)
      ])]
    });
  }

  private onLoginSuccess(profile: Guest) {
    this.localStorageManager.setUsername(profile);
    this.router.navigateByUrl('');
  }

  private handleError(error: HttpErrorResponse) {
    this.error = true;
    this.invalidCredential = error.status === 403;
    this.form.reset();
  }

}
