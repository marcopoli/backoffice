import { Component, OnInit } from '@angular/core';
import { CONSTANTS } from 'src/assets/consts/app.const';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  project = CONSTANTS.project;
  year = CONSTANTS.year;
  imgLoghi = 'assets/image/loghi_feelathome.png';

  constructor() { }

  ngOnInit() {
  }

}
