import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from '../services/data-storage-services/authentication.service';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [AuthenticationService],
  bootstrap: []
})
export class AuthModule { }
