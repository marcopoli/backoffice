pipeline {
    agent { label 'master' }
    environment {
        VERSION = """${sh(
                returnStdout: true,
                script: 'npm run version --silent'
            )}"""
    }
    stages {
        stage('Build') {
            steps {
                sh 'npm install'
            }
        }
        stage('CheckStyle') {
            steps {
                sh 'tslint --force -c tslint_checkstyle.json "**/*.ts" -o checkstyle-result.xml -t checkstyle'
            }
        }
        stage('SonarQube Stage') {
            when { branch "master" }
            steps {
                withSonarQubeEnv ('SonarQube') {
                    sh '/var/jenkins_home/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQubeScanner/bin/sonar-scanner -Dsonar.projectKey=feelathome-backoffice-stage -Dsonar.sources=. -Dsonar.host.url=http://172.30.200.236:9000 -Dsonar.login=d0108573e47f8b3ca44e1bd3c9380ff7486a89a4'
                }
            }
        }
        stage("Quality Gate") {
            when { branch "master" }
            steps {
              timeout(time: 10, unit: 'MINUTES') {
                waitForQualityGate abortPipeline: true
              }
            }
          }
        stage('Docker Login') {
            steps {
                sh 'docker login registry.gitlab.com --username a.leovino@eminds.it --password LvnLsn81D'
            }
        }
        stage('Docker Build Stage') {
            when { branch "master" }
            steps {
                sh 'docker build -t registry.gitlab.com/feel-at-home/backoffice:stage_${VERSION} --build-arg PROD=false .'
            }
        }
        stage('Docker Build PreProduction') {
            when { branch "preproduction" }
            steps {
                sh 'docker build -t registry.gitlab.com/feel-at-home/backoffice:preproduction_${VERSION} .'
            }
        }
        stage('Docker Build Production') {
            when { branch "production" }
            steps {
                sh 'docker build -t registry.gitlab.com/feel-at-home/backoffice:${VERSION} --build-arg PROD=true .'
            }
        }
        stage('Docker Push Stage') {
            when { branch "master" }
            steps {
                sh 'docker push registry.gitlab.com/feel-at-home/backoffice:stage_${VERSION}'
            }
        }
        stage('Docker Push PreProduction') {
            when { branch "preproduction" }
            steps {
                sh 'docker push registry.gitlab.com/feel-at-home/backoffice:preproduction_${VERSION}'
            }
        }
        stage('Docker Push Production') {
            when { branch "production" }
            steps {
                sh 'docker push registry.gitlab.com/feel-at-home/backoffice:${VERSION}'
            }
        }
    }
    post {
            success {
                slackSend (channel: 'feelathome-backoffice', color: '#00FF00', message: "SUCCESSFUL ${VERSION}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
            failure {
                slackSend (channel: 'feelathome-backoffice', color: '#FF0000', message: "FAILURE ${VERSION}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
            always {
                recordIssues enabledForFailure: true, tool: checkStyle()
                slackSend (channel: 'feelathome', color: '#FFFF00', message: "Backoffice ${VERSION}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
    }
}
